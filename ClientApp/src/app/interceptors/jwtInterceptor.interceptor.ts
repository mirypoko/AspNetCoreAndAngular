import { Observable } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from '../services/core/authentication-service.service';
import { JwtTokensService } from '../services/core/jwt-tokens.service';


const apiUrl = '/api/authentication'

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    private _authService: AuthenticationService;

    private _jwtTokensService: JwtTokensService;

    constructor(private _inj: Injector) {
        this._authService = _inj.get(AuthenticationService);
        this._jwtTokensService = _inj.get(JwtTokensService);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (request.url == '/api/authentication/UpdateJwtToken') {
            return next.handle(request);
        }

        if (this._authService.UseJwtBearer) {
            let token = this._jwtTokensService.getJwtTokenFromLocalStorage();
            if (token == null) {
                return next.handle(request);
            } else {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ` + token.accessToken
                    }
                });
                return next.handle(request);
            }
        } else {
            return next.handle(request);
        }

    }
}
