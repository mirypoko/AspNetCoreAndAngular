import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Material
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatFormFieldModule,

  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher,
  MatTreeModule,
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/core/app/app.component';
import { MainNavComponent } from './components/core/main-nav/main-nav.component';
import { HomeComponent } from './components/core/home/home.component';
import { UserMenuComponent } from './components/core/user/user-menu/user-menu.component';
import { SignUpComponent } from './components/core/user/sign-up/sign-up.component';
import { SettingsComponent } from './components/core/user/settings/settings.component';
import { LogInComponent } from './components/core/user/log-in/log-in.component';
import { ConfirmComponent } from './components/core/confirm/confirm.component';
import { AdminGuard } from './guards/admin.guard';
import { AuthorizationGuard } from './guards/authorization.guard';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './interceptors/jwtInterceptor.interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilmsListComponent } from './components/films/films-list/films-list.component';
import { AddFilmComponent } from './components/films/add-film/add-film.component';
import { PageNotFoundComponent } from './components/core/page-not-found/page-not-found.component';
import { DetailsFilmComponent } from './components/films/details-film/details-film.component';

import { EqualValidator } from './validators/equal.validator';

import { UserService } from './services/core/user.service';
import { SnackBarsServiceService } from './services/core/snack-bars-service.service';
import { AuthenticationService } from './services/core/authentication-service.service';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MainNavComponent,
    HomeComponent,
    UserMenuComponent,
    SignUpComponent,
    SettingsComponent,
    LogInComponent,
    ConfirmComponent,
    FilmsListComponent,
    AddFilmComponent,
    DetailsFilmComponent,

    // Validators
    EqualValidator,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    FormsModule,
    ReactiveFormsModule,

    HttpClientModule,

    // Material
    MatFormFieldModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  entryComponents: [
    LogInComponent,
    SignUpComponent,
    ConfirmComponent,
    SettingsComponent,
    AddFilmComponent
  ],
  providers: [
    {
      provide: ErrorStateMatcher,
      useClass: ShowOnDirtyErrorStateMatcher
    },
    // Services
    UserService,
    SnackBarsServiceService,
    AuthenticationService,
    // Interceptors
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    // Guards
    AuthorizationGuard,
    AdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
