import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable, Subscription } from 'rxjs';
import { IUser } from '../models/user/user';
import { AuthenticationService } from '../services/core/authentication-service.service';

@Injectable()
export class AdminGuard implements CanActivate {

  currentUserSubscription: Subscription;

  currentUser: IUser = null;

  currentUserIsAdmin: boolean = false;


  constructor(
    private _router: Router,
    private _authService: AuthenticationService
  ) {
    this.currentUserSubscription = this._authService.CurrentUser.subscribe(
      result => {
        this.currentUser = result;
        this.currentUserIsAdmin = this._authService.currentUserIsAdmin();
      }
    );
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable<boolean>(
      sub => {
        sub.next(this.currentUserIsAdmin);
      }
    );
  }
}