import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, } from 'rxjs';
import { AuthenticationService } from '../services/core/authentication-service.service';

@Injectable()
export class AuthorizationGuard implements CanActivate {

  constructor(
    private _authService: AuthenticationService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._authService.userIsAuthorized();
  }
}
