import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/core/home/home.component';
import { FilmsListComponent } from './components/films/films-list/films-list.component';
import { PageNotFoundComponent } from './components/core/page-not-found/page-not-found.component';
import { DetailsFilmComponent } from './components/films/details-film/details-film.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'films', component: FilmsListComponent },
  { path: 'films/details/:id', component: DetailsFilmComponent },
  { path: '**', component: PageNotFoundComponent },
  { path: '404', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
