import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';
import { IUser } from 'src/app/models/user/user';
import { SignUpComponent } from '../user/sign-up/sign-up.component';
import { LogInComponent } from '../user/log-in/log-in.component';

/** @title Responsive sidenav */
@Component({
  selector: 'app-main-nav',
  templateUrl: 'main-nav.component.html',
  styleUrls: ['main-nav.component.css'],
})
export class MainNavComponent implements OnDestroy {
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  currentUser: IUser = null;

  currentUserIsAdmin: boolean = false;

  currentUserSubscription: Subscription;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private dialog: MatDialog,
    private _authService: AuthenticationService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.currentUserSubscription = this._authService.CurrentUser.subscribe(
      result => {
        this.currentUser = result;
        this.currentUserIsAdmin = this._authService.currentUserIsAdmin();
      });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.currentUserSubscription.unsubscribe();
  }

  openSingUpDialog() {
    this.dialog.open(SignUpComponent).afterClosed().subscribe(
      result => {
        if (result) {
          this.openLoginDialog();
        }
      });
  }

  openLoginDialog(): void {
    this.dialog.open(LogInComponent).afterClosed().subscribe(
      result => {
        if (result) {
          this.openSingUpDialog();
        }
      });
  }
}
