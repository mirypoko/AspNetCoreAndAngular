import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SnackBarsServiceService } from 'src/app/services/core/snack-bars-service.service';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';
import { ILogIn } from 'src/app/models/authorization/logIn';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  constructor(private _authenticationService: AuthenticationService,
    private _snackBarsService: SnackBarsServiceService,
    private _dialog: MatDialog,
    private _thisDialogRef: MatDialogRef<LogInComponent>) {
  }

  form = new FormGroup({
    "userName": new FormControl("", [Validators.required, Validators.minLength(4), Validators.pattern("^[0-9]*[A-Za-z][A-Za-z0-9]*$")]),
    "password": new FormControl("", [Validators.required, Validators.minLength(8)]),
  });

  loading: boolean = false;

  ngOnInit() {
  }

  submit() {
    if (!this.form.valid) {
      this._snackBarsService.showError("Fill all fields.");
      return;
    }

    let formData = this.form.value;

    let singInObject: ILogIn = {
      userName: formData.userName,
      password: formData.password
    };

    this.login(singInObject);
  }

  protected SetIsBusy(isBusy: boolean){
    this.loading = isBusy;
    let cdkBackdrops = document.getElementsByClassName('cdk-overlay-backdrop');
    if (isBusy) {
      document.body.style.pointerEvents = 'none';
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.add('pointer-events-none');
      }
    } else {
      document.body.style.pointerEvents = 'auto'
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.remove('pointer-events-none');
      }
    }
  }

  protected login(singInObject: ILogIn) {
    this.SetIsBusy(true);

    this._authenticationService.login(singInObject).subscribe(
      result => {
        this.SetIsBusy(false);
        this._dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.SetIsBusy(false);
        if (error == undefined) {
          this._snackBarsService.showServerErrorSnackBar(error);
          return;
        }
        if (error.status == 403) {
          this._snackBarsService.showError("Invalid password.");
          return;
        }
        if (error.status == 404) {
          this._snackBarsService.showError("User with received email or name not found.");
          return;
        }
      }
    );
  }


  close() {
    this._thisDialogRef.close(false);
  }

  openSignUpDialog() {
    this._thisDialogRef.close(true);
  }
}
