import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/models/user/user';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { SettingsComponent } from '../settings/settings.component';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';
import { UserService } from 'src/app/services/core/user.service';
import { ConfirmComponent } from '../../confirm/confirm.component';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {

  currentUser: IUser = null;

  currentUserSubscription: Subscription;

  constructor(
    private _authenticationService: AuthenticationService,
    private _userService: UserService,
    private _dialog: MatDialog) { }

  openSettings(){
    let dialogRef = this._dialog.open(SettingsComponent);
  }

  logout() {
    let dialogRef = this._dialog.open(ConfirmComponent);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this._authenticationService.logout();
        }
      }
    )
  }

  ngOnInit() {
    this.currentUserSubscription = this._authenticationService.CurrentUser.subscribe(
      result => this.currentUser = result
    );
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }

}
