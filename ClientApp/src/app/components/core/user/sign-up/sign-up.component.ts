import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/services/core/user.service';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';
import { SnackBarsServiceService } from 'src/app/services/core/snack-bars-service.service';
import { IPostUser } from 'src/app/models/user/postUser';
import { ILogIn } from 'src/app/models/authorization/logIn';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private _userService: UserService,
    private _authenticationService: AuthenticationService,
    private _snackBarsService: SnackBarsServiceService,
    private _thisDialogRef: MatDialogRef<SignUpComponent>) {
  }

  loading: boolean = false;


  ngOnInit() {
  }

  form = new FormGroup({
    "username": new FormControl("", [Validators.required, Validators.minLength(4), Validators.pattern("^[0-9]*[A-Za-z][A-Za-z0-9]*$")]),
    "email": new FormControl("", [Validators.required, Validators.email]),
    "password": new FormControl("", [Validators.required, Validators.minLength(8)]),
    "confirmPassword": new FormControl("", [Validators.required])
  });


  submit() {

    if (!this.form.valid) {
      this._snackBarsService.showError('Fill all fields');
      return;
    }

    let formData = this.form.value;
    let postUser: IPostUser = {
      username: formData.username,
      email: formData.email,
      password: formData.password
    };

    this.SetIsBusy(true);
    this._userService.postUser(postUser).subscribe(
      result => {
        var singInObj: ILogIn = {
          userName: postUser.username,
          password: postUser.password
        }
        this.login(singInObj);
      },
      (error: HttpErrorResponse) => {
        if (error.status == 400) {
          this._snackBarsService.showError(error.error);
        } else {
          this._snackBarsService.showServerErrorSnackBar(error);
        }
        this.SetIsBusy(false);
      }
    );
  }

  protected login(singInObject: ILogIn) {
    this._authenticationService.login(singInObject).subscribe(
      result => {
        this.SetIsBusy(false);
        this.close();
      },
      (error: HttpErrorResponse) => {
        if (error.status == 400) {
          this._snackBarsService.showError("Invalid password.")
          return;
        }
        if (error.status == 404) {
          this._snackBarsService.showError("User with received email or name not found.")
          return;
        }
        this._snackBarsService.showServerErrorSnackBar(error);
        this.SetIsBusy(false);
      }
    );
  }

  close() {
    this._thisDialogRef.close(false);
  }

  openLogInpDialog() {
    this._thisDialogRef.close(true);
  }

  protected SetIsBusy(isBusy: boolean){
    this.loading = isBusy;
    let cdkBackdrops = document.getElementsByClassName('cdk-overlay-backdrop');
    if (isBusy) {
      document.body.style.pointerEvents = 'none';
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.add('pointer-events-none');
      }
    } else {
      document.body.style.pointerEvents = 'auto'
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.remove('pointer-events-none');
      }
    }
  }
}
