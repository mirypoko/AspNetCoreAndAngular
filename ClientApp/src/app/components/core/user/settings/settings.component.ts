import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/services/core/user.service';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';
import { SnackBarsServiceService } from 'src/app/services/core/snack-bars-service.service';
import { IUser } from 'src/app/models/user/user';
import { IPutUser } from 'src/app/models/user/putUser';
import { ILogIn } from 'src/app/models/authorization/logIn';
import { IPutUserPassword } from 'src/app/models/user/putUserPassword';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  currentUser: IUser = null;

  currentUserSubscription: Subscription;

  loading: boolean = false;

  form = new FormGroup({
    "userName": new FormControl("", [Validators.required, Validators.minLength(4), Validators.pattern("^[0-9]*[A-Za-z][A-Za-z0-9]*$")]),
    "email": new FormControl("", [Validators.required, Validators.email]),
    "currentPassword": new FormControl("", [Validators.required, Validators.minLength(8)]),
    "password": new FormControl("", [Validators.minLength(8)]),
    "confirmPassword": new FormControl("", [Validators.minLength(8)])
  });


  constructor(private _userService: UserService,
    private _authenticationService: AuthenticationService,
    private _snackBarsService: SnackBarsServiceService,
    private _thisDialogRef: MatDialogRef<SettingsComponent>) {
  }

  ngOnInit() {
    this.currentUserSubscription = this._authenticationService.CurrentUser.subscribe(
      result => {
        if (result != null) {
          this.currentUser = result;
          this.form.controls['userName'].setValue(result.userName);
          this.form.controls['email'].setValue(result.email);
        }
      }
    );
  }

  submit() {
    if (!this.form.valid) {
      this._snackBarsService.showError('Fill in required fields');
      return;
    }

    let formData = this.form.value;
    let putUser: IPutUser = {
      id: this.currentUser.id,
      userName: formData.userName,
      email: formData.email,
      currentPassword: formData.currentPassword
    };

    if (formData.password.length > 0) {
      if (formData.password != formData.confirmPassword) {
        this._snackBarsService.showError('The new password does not match confirmation');
        return;
      }
    }

    this.SetIsBusy(true);

    this._userService.putUser(putUser).subscribe(
      result => {
        let login: ILogIn = {
          userName: putUser.userName,
          password: putUser.currentPassword
        }
        this._authenticationService.login(login).subscribe(
          result => {
            if (formData.password.length > 0) {
              this.putPasswordAndSingIn(formData.userName, formData.currentPassword, formData.password)
            } else {
              this._snackBarsService.showSuccess('Data updated');
              this.SetIsBusy(false);
            }
          },
          (error: HttpErrorResponse) => {
            if (error.status == 400) {
              this._snackBarsService.showError(error.error);
            } else {
              this._snackBarsService.showServerErrorSnackBar(error);
            }
            this.SetIsBusy(false);
          }
        );
      },
      (error: HttpErrorResponse) => {
        if (error.status == 400) {
          this._snackBarsService.showError(error.error);
        } else {
          this._snackBarsService.showServerErrorSnackBar(error);
        }
        this.SetIsBusy(false);
      }
    );
  }

  protected putPasswordAndSingIn(userName: string, oldPassword: string, newPassword: string) {
    let putUserPassword: IPutUserPassword = {
      id: this.currentUser.id,
      newPassword: newPassword,
      currentPassword: oldPassword
    }

    this._userService.putPassword(putUserPassword).subscribe(
      result => {
        let singInObject: ILogIn = {
          userName: userName,
          password: newPassword
        };
        this.login(singInObject);
        this._snackBarsService.showSuccess('Data updated');
        this.SetIsBusy(false);
      },
      (error: HttpErrorResponse) => {
        if (error.status == 400) {
          this._snackBarsService.showError(error.error);
          return;
        } else {
          this._snackBarsService.showServerErrorSnackBar(error);
        }
        this.SetIsBusy(false);
      }
    );
  }

  close() {
    this._thisDialogRef.close();
  }

  protected clearPasswordData() {
    this.form.controls['currentPassword'].reset();
    this.form.controls['currentPassword'].setValue('');
    this.form.controls['password'].reset();
    this.form.controls['password'].setValue('');
    this.form.controls['confirmPassword'].reset();
    this.form.controls['confirmPassword'].setValue('');
  }

  protected login(singInObject: ILogIn) {
    this._authenticationService.login(singInObject).subscribe(
      result => {

      },
      (error: HttpErrorResponse) => {
        if (error.status == 400) {
          this._snackBarsService.showError("Invalid password.");
          return;
        }
        if (error.status == 404) {
          this._snackBarsService.showError("User with received email or name not found.");
          return;
        }
        this._snackBarsService.showServerErrorSnackBar(error);
      }
    );
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }

  protected SetIsBusy(isBusy: boolean){
    this.loading = isBusy;
    let cdkBackdrops = document.getElementsByClassName('cdk-overlay-backdrop');
    if (isBusy) {
      document.body.style.pointerEvents = 'none';
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.add('pointer-events-none');
      }
    } else {
      document.body.style.pointerEvents = 'auto'
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.remove('pointer-events-none');
      }
    }
  }

}
