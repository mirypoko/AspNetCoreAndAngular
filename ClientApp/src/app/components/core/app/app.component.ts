import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';
import { JwtTokensService } from 'src/app/services/core/jwt-tokens.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'films';

  loading: boolean = true;

  constructor(
    private _authService: AuthenticationService,
    private _jwtService: JwtTokensService) {

    if (this._jwtService.getJwtTokenFromLocalStorage() != null) {
      this._authService.userIsAuthorized().subscribe(
        authorized => {
          if (authorized) {
            this._jwtService.getNewJwtByRefreshToken().subscribe(
              jwt => {
                this._jwtService.setJwtTokenInToLocalStorage(jwt);
                this._authService.loadCurrentUserFromServer().subscribe(
                  next =>{this.loading = false;},
                  error => {this.loading = false;},
                );
              }, error => {
                this._jwtService.deleteJwtFromLocalStorage();
                this.loading = false;
              }
            );
          }
        }
      )
    } else {
      this.loading = false;
    }
  }

  ngOnInit() {
  }
}
