import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FilmsService } from 'src/app/services/films.service';
import { MatDialog } from '@angular/material';
import { IGetFilm } from 'src/app/models/films/getFilm';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmComponent } from '../../core/confirm/confirm.component';
import { HttpErrorResponse } from '@angular/common/http';
import { PlatformLocation } from '@angular/common';
import { SnackBarsServiceService } from 'src/app/services/core/snack-bars-service.service';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';

@Component({
  selector: 'app-details-film',
  templateUrl: './details-film.component.html',
  styleUrls: ['./details-film.component.css']
})
export class DetailsFilmComponent implements OnInit {

  loading = true;

  entity: IGetFilm;

  currentUserIsAdmin = false;

  currentUserSubscription: Subscription;

  constructor(private _dataService: FilmsService,
    private _snackBarsService: SnackBarsServiceService,
    private _authService: AuthenticationService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog) {
  }

  ngOnInit() {

    this.loading = true;

    this.currentUserSubscription = this._authService.CurrentUser.subscribe(
      result => {
        this.currentUserIsAdmin = this._authService.currentUserIsAdmin();
      });

    let id = parseInt(this._route.snapshot.paramMap.get('id'));
    if (isNaN(id)) {
      this._router.navigate(['/404']);
      this.loading = false;
    }

    if (id < 1) {
      this._router.navigate(['/404']);
      this.loading = false;
    } else {
      this._dataService.getById(id).subscribe(
        result => {
          this.entity = result;
          this.loading = false;
        },
        error => this._snackBarsService.showServerErrorSnackBar(error)
      )
    }
  };

  delete() {
    let dialogRef = this._dialog.open(ConfirmComponent);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this.loading = true;
          this._dataService.delete(this.entity.id).subscribe(
            result => {
              this._router.navigate(['/films']);
              this.loading = false;
            },
            (error: HttpErrorResponse) => {
              this._snackBarsService.showError(error.error);
              this.loading = false;
            });
        }
      }
    )
  }
}
