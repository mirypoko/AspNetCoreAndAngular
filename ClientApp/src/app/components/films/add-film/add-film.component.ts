import { Component, OnInit, ViewChild } from '@angular/core';
import { FilmsService } from '../../../services/films.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { IPostFilm } from 'src/app/models/films/postFilm';
import { SnackBarsServiceService } from 'src/app/services/core/snack-bars-service.service';

@Component({
  selector: 'app-add-film',
  templateUrl: './add-film.component.html',
  styleUrls: ['./add-film.component.css']
})
export class AddFilmComponent implements OnInit {

  @ViewChild("fileInput") fileInput;

  photoName: string = 'Chose image...';

  loading: boolean = false;

  tryToSubmit: boolean = false;

  constructor(
    private _dataService: FilmsService,
    private _snackBarsService: SnackBarsServiceService,
    private _thisDialogRef: MatDialogRef<AddFilmComponent>
  ) {
  }

  form = new FormGroup({
    "name": new FormControl("", [Validators.required]),
    "cover": new FormControl("", [Validators.required]),
    "description": new FormControl("", [Validators.required]),
    "director": new FormControl("", [Validators.required]),
    "year": new FormControl("", [Validators.required, Validators.min(1895), Validators.max((new Date()).getFullYear())])
  });

  submit() {
    this.tryToSubmit = true;
    if (!this.form.valid) {
      this._snackBarsService.showError('Fill in required fields');
      return;
    }

    this.SetIsBusy(true);

    let formData = this.form.value;
    let entity: IPostFilm = {
      name: formData.name,
      cover: formData.cover,
      description: formData.description,
      director: formData.director,
      year: formData.year
    };

    this._dataService.post(entity).subscribe(
      result => {
        this.SetIsBusy(false);
        this._snackBarsService.showSuccess("Added");
        this.close(true);
      },
      (error: HttpErrorResponse) => {
        this.SetIsBusy(false);
        if (error.status == 400) {
          this._snackBarsService.showError(error.error);
        } else {
          this._snackBarsService.showServerErrorSnackBar(error);
        }
      }
    );
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.form.get('cover').setValue(file);
      this.photoName = file.name;
      if (this.photoName.length > 26) {
        this.photoName = this.photoName.substr(0, 23) + "...";
      }
    }
  }

  ngOnInit() {
  }


  close(result: boolean) {
    this._thisDialogRef.close(result);
  }

  protected SetIsBusy(isBusy: boolean){
    this.loading = isBusy;
    let cdkBackdrops = document.getElementsByClassName('cdk-overlay-backdrop');
    if (isBusy) {
      document.body.style.pointerEvents = 'none';
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.add('pointer-events-none');
      }
    } else {
      document.body.style.pointerEvents = 'auto'
      for (let i = 0; i < cdkBackdrops.length; i++) {
        cdkBackdrops[i].classList.remove('pointer-events-none');
      }
    }
  }
}
