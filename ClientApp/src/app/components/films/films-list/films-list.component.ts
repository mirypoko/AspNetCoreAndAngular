import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FilmsService } from '../../../services/films.service';
import { IGetFilm } from '../../../models/films/getFilm';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription, Subject } from 'rxjs';
import { PageEvent, MatDialog } from '@angular/material';
import { AddFilmComponent } from '../add-film/add-film.component';
import { debounceTime } from 'rxjs/operators';
import { ConfirmComponent } from '../../core/confirm/confirm.component';
import { SnackBarsServiceService } from 'src/app/services/core/snack-bars-service.service';
import { AuthenticationService } from 'src/app/services/core/authentication-service.service';


@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.css']
})
export class FilmsListComponent implements OnInit, AfterViewInit {

  loading = true;

  entities: Array<IGetFilm> = [];

  currentUserIsAdmin = false;

  currentUserSubscription: Subscription;

  filter = '';

  private filterSubject: Subject<string> = new Subject();

  length = 100;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  pageEvent: PageEvent = new PageEvent();

  constructor(
    private _dataService: FilmsService,
    private _snackBarsService: SnackBarsServiceService,
    private _authService: AuthenticationService,
    private _dialog: MatDialog) {
    this.pageEvent.pageIndex = 0;
    this.pageEvent.pageSize = 10;
    this.pageEvent.length = 100;
  }

  onPageEvent() {
    this.pageSize = this.pageEvent.pageSize;
    this.loadData()
  }

  applyFilter() {
    let filter = this.filter.trim();
    filter = this.filter.toLowerCase();

    if (filter.length == 0) {
      this.loadFirstPage();
      return;
    }

    if (filter.length < 2) {
      return;
    }

    this.loadData();
  }

  openAddDialog(): void {
    this._dialog.open(AddFilmComponent).afterClosed().subscribe(
      result => {
        if (result) {
          this.loadFirstPage();
        }
      }
    );
  }

  ngOnInit() {

    this.filterSubject.pipe(debounceTime(500)).subscribe(searchTextValue => {
      this.applyFilter();
    });

    this.currentUserSubscription = this._authService.CurrentUser.subscribe(
      result => {
        this.currentUserIsAdmin = this._authService.currentUserIsAdmin();
      });
  }

  ngAfterViewInit(): void {
    this.loadFirstPage();
  }

  onFilterKeyUp() {
    this.filterSubject.next(this.filter);
  }

  delete(id: number) {
    let dialogRef = this._dialog.open(ConfirmComponent);
    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this._dataService.delete(id).subscribe(
            result => {
              this.loadFirstPage();
            },
            (error: HttpErrorResponse) => {
              this._snackBarsService.showError(error.error);
            });
        }
      }
    );
  }

  loadFirstPage() {
    this.pageEvent.pageIndex = 0;
    this.loadData();
  }

  loadData() {
    this.loading = true;
    this._dataService.get(this.pageEvent.pageSize, this.filter, this.pageEvent.pageIndex * this.pageEvent.pageSize).subscribe(
      films => {
        this.entities = films;
        this.loading = false;
        this._dataService.getSearchCount(this.filter).subscribe(
          count => {
            this.length = count;
          },
          (error: HttpErrorResponse) => {
            this._snackBarsService.showError(error.error);
          }
        );
      },
      (error: HttpErrorResponse) => {
        this._snackBarsService.showError(error.error);
        this.loading = false;
      }
    );
  }
}
