import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IGetFilm } from '../models/films/getFilm';
import { IPutFilm } from '../models/films/putFilm';
import { IPostFilm } from '../models/films/postFilm';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  protected apiUrl = 'api/films';

  constructor(protected _httpClient: HttpClient) { }

  public getSearchCount(searchString: string): Observable<number> {
    if (searchString == null || searchString.length < 2) {
      return this.getCount();
    }
    let params = new HttpParams();
    if (searchString != null && searchString.length > 2) {
      params = params.append('searchString', searchString);
    }
    return this._httpClient.get<number>(this.apiUrl + '/SearchCount', { params: params });
  }

  public getCount(): Observable<number> {
    return this._httpClient.get<number>(this.apiUrl + '/Count');
  }

  public get(count: number = 0, searchString: string, offset: number = 0): Observable<Array<IGetFilm>> {
    let params = new HttpParams();
    if (offset > 0) {
      params = params.append('offset', offset.toString());
    }
    if (searchString != null && searchString.length > 2) {
      params = params.append('searchString', searchString);
    }
    if (count > 0) {
      params = params.append('count', count.toString());
    }
    return this._httpClient.get<Array<IGetFilm>>(this.apiUrl, { params: params });
  }

  public getById(id: number): Observable<IGetFilm> {
    return this._httpClient.get<IGetFilm>(this.apiUrl + '/' + id);
  }

  public put(entity: IPutFilm) {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.put(this.apiUrl, JSON.stringify(entity), { headers: httpHeaders });
  }

  public post(entity: IPostFilm): Observable<IGetFilm> {
    let data = new FormData();
    data.append("cover", entity.cover);
    data.append('description', entity.description);
    data.append('director', entity.director);
    data.append('name', entity.name);
    data.append('year', '' + entity.year);

    return this._httpClient.post<IGetFilm>(this.apiUrl, data);
  }

  public delete(id: number) {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.delete(this.apiUrl + '/' + id);
  }
}
