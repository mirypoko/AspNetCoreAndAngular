import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IJwtToken } from '../../models/authorization/jwtToken';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ILogIn } from '../../models/authorization/logIn';

const apiUrl = '/api/authentication';

@Injectable({
  providedIn: 'root'
})
export class JwtTokensService {

  private _refreshAccessTokenIntervalId: any;

  constructor(private _httpClient: HttpClient) {
     }

  public getJwtTokenFromLocalStorage(): IJwtToken {
    let jwtTokenJson = localStorage.getItem('jwtToken');
    if (jwtTokenJson) {
      let jwtToken: IJwtToken = JSON.parse(jwtTokenJson);
      return jwtToken;
    } else {
      return null;
    }
  }

  public setJwtTokenInToLocalStorage(jwtToken: IJwtToken): void {
    localStorage.setItem('jwtToken', JSON.stringify(jwtToken));
  }

  public getNewJwtByRefreshToken(): Observable<IJwtToken> {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.post<IJwtToken>(apiUrl + '/UpdateJwtToken', JSON.stringify(this.getJwtTokenFromLocalStorage().refreshToken), { headers: httpHeaders });
  }

  public getJwtTokenFromServer(singIn: ILogIn): Observable<IJwtToken> {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.post<IJwtToken>(apiUrl + '/GetJwtToken', JSON.stringify(singIn), { headers: httpHeaders });
  }

  public deleteJwtFromLocalStorage() {
    localStorage.removeItem('jwtToken');
  }

  public runGetJwtTokenSheduler() {
    this._refreshAccessTokenIntervalId = setInterval(() => {
      this.refreshJwtTokenAction();
    }, (this.getJwtTokenFromLocalStorage().lifetime * 60000) - 10000);
  }

  public stopGetAccessjwtTokenSheduler() {
    clearTimeout(this._refreshAccessTokenIntervalId);
  }

  private refreshJwtTokenAction() {
    if (this.getJwtTokenFromLocalStorage() != null) {
      this.getNewJwtByRefreshToken().subscribe(
        result => {
          this.setJwtTokenInToLocalStorage(result);
        },
        (error: HttpErrorResponse) => {
          if (error.status >= 400 && 500 < error.status) {
            this.deleteJwtFromLocalStorage();
            this.stopGetAccessjwtTokenSheduler();
          }
        }
      )
    } else {
      this.stopGetAccessjwtTokenSheduler();
    }
  }

}
