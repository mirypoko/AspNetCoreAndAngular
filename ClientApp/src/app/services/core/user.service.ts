import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { IUser } from '../../models/user/user';
import { IPutUserPassword } from '../../models/user/putUserPassword';
import { IPutUser } from '../../models/user/putUser';
import { IPostUser } from '../../models/user/postUser';
import { AuthenticationService } from './authentication-service.service';

const apiUrl = 'api/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(
    private _httpClient: HttpClient,
    private _authenticationService: AuthenticationService
    ) { }

  public getUsers(): Observable<Array<IUser>> {
    return this._httpClient.get<Array<IUser>>(apiUrl);
  }

  public getUser(id: string): Observable<IUser> {
    return this._httpClient.get<IUser>(apiUrl + id);
  }

  public putPassword(model: IPutUserPassword) {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.put(apiUrl + '/password', JSON.stringify(model), { headers: httpHeaders });
  }

  public putUser(user: IPutUser) {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.put<IUser>(apiUrl + '/' + this._authenticationService.CurrentUser.value.id, JSON.stringify(user), { headers: httpHeaders });
  }

  public postUser(user: IPostUser) {
    let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._httpClient.post(apiUrl, JSON.stringify(user), { headers: httpHeaders });
  }
}
