import { TestBed, inject } from '@angular/core/testing';

import { JwtTokensService } from './jwt-tokens.service';

describe('JwtTokensService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JwtTokensService]
    });
  });

  it('should be created', inject([JwtTokensService], (service: JwtTokensService) => {
    expect(service).toBeTruthy();
  }));
});
