import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Router } from '@angular/router';
import { IUser } from '../../models/user/user';
import { ServiceResult } from '../../models/serviceResult';
import { ILogIn } from '../../models/authorization/logIn';
import { JwtTokensService } from './jwt-tokens.service';

const apiUrl = '/api/authentication';

const apiUrlUsers = 'api/users';


@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private _useJwtBearer: boolean = true;
    public get UseJwtBearer(): boolean {
        return this._useJwtBearer;
    }

    private _currentUser: BehaviorSubject<IUser> = new BehaviorSubject(null);
    public get CurrentUser(): BehaviorSubject<IUser> {
        return this._currentUser;
    }

    constructor(
        private _httpClient: HttpClient,
        private _router: Router,
        private _jwtService: JwtTokensService) {
    }

    public userInRole(role: string): boolean {
        if (this._currentUser.value == null) {
            return false;
        } else {
            return this._currentUser.value.roles.indexOf(role) != -1;
        }
    }

    public currentUserIsAdmin(): boolean {
        return this.userInRole('ADMIN');
    }

    public userIsAuthorized(): Observable<boolean> {
        return new Observable<boolean>(
            sub => {
                if (this.UseJwtBearer) {
                    sub.next(this._jwtService.getJwtTokenFromLocalStorage() != null);
                } else {
                    this._httpClient.get<boolean>(apiUrl + "/IsAuthenticated").subscribe(
                        result => {
                            sub.next(result);
                        },
                        error => sub.error(error)
                    );
                }
            }
        )

    }

    public login(login: ILogIn): Observable<any> {
        if (this.UseJwtBearer) {
            return this.loginJwt(login);
        } else {
            return this.loginCookes(login);
        }
    }

    public logout(): void {
        if (this.UseJwtBearer) {
            this.logoutJwt();
            this._currentUser.next(null);
            this._router.navigate(['/home']);
        } else {
            this.logoutCookies()
                .subscribe(
                    result => {
                        this._currentUser.next(null);
                        this._router.navigate(['/home']);
                    },
                    error => {
                        console.error(error);
                    }
                );
        }
    }

    private loginCookes(singIn: ILogIn): Observable<any> {
        return new Observable<any>(
            sub => {
                let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
                this._httpClient.post(apiUrl + '/Cookies', JSON.stringify(singIn), { headers: httpHeaders })
                    .subscribe(
                        result => {
                            this.loadCurrentUserFromServer().subscribe(
                                r => sub.next(true),
                                error => sub.error(error)
                            );
                        },
                        error => sub.error(error)
                    );
            }
        );
    }

    private loginJwt(login: ILogIn): Observable<any> {
        return new Observable<any>(
            sub => {
                this._jwtService.getJwtTokenFromServer(login).subscribe(
                    result => {
                        this._jwtService.setJwtTokenInToLocalStorage(result);
                        this._jwtService.runGetJwtTokenSheduler();
                        this.loadCurrentUserFromServer().subscribe(
                            r => sub.next(true),
                            error => sub.error(error)
                        );
                    }, error => {
                        sub.error(error);
                    }
                )
            }
        )
    }


    private logoutCookies(): Observable<any> {
        let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this._httpClient.post(apiUrl + '/logout', null);
    }

    private logoutJwt(): void {
        this._jwtService.stopGetAccessjwtTokenSheduler();
        this._jwtService.deleteJwtFromLocalStorage();
    }

    public loadCurrentUserFromServer(): Observable<boolean> {
        return new Observable<boolean>(
            sub => {
                this.getCurrentUserFromServer().subscribe(
                    user => {
                        this._currentUser.next(user);
                        sub.next(true);
                    }, error => {
                        sub.next(false);
                    }
                )
            }
        );
    }

    public getCurrentUserFromServer(): Observable<IUser> {
        return this._httpClient.get<IUser>(apiUrlUsers + '/current');
    }
}