export interface IJwtToken{
    accessToken: string;
    refreshToken: string;
    userId: string;
    expires: number;
    lifetime: number;
}