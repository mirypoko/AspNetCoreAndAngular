export interface IGetFilm{
    name: string;
    coverUrl: string;
    description: string;
    director: string;
    year: number;
    id: number;
}