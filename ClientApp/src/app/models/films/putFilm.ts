export interface IPutFilm{
    id: number;
    name: string;
    Cover: any;
    description: string;
    director: string;
    year: number;
}