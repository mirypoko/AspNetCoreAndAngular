export interface IPostFilm{
    name: string;
    cover: any;
    description: string;
    director: string;
    year: number;
}