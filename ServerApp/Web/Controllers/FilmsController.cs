﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Core.Constants;
using Domain.Core.Entities;
using Domain.Core.Enums;
using Domain.Core.Models;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Core;
using Services.Interfaces;
using Services.Interfaces.Core;
using Web.Core.Controllers;
using Web.Core.Extensions;
using Web.ViewModels;
using Web.ViewModels.Film;

namespace Web.Controllers
{

    /// <inheritdoc />
    [Authorize(Roles = Roles.Admin)]
    [Route("api/[controller]")]
    public class FilmsController : ControllerBase, IRestController<int, Film, FilmPostViewModel, FilmPutViewModel, GetFilmFilterViewModel>
    {
        private readonly IFilesServiceCore _fileService;

        private readonly IFilmsService _filmsService;

        private readonly ILogger<FilmsController> _logger;

        public FilmsController(IFilmsService filmsService, ILogger<FilmsController> logger, IFilesServiceCore fileService)
        {
            _filmsService = filmsService;
            _logger = logger;
            _fileService = fileService;
        }

        /// <summary>
        /// Search films.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Failed to get films. Error in response body.</response>
        [AllowAnonymous]
        [HttpGet]
        [ProducesResponseType(typeof(List<FilmGetViewModel>), 200)]
        [ProducesResponseType(typeof(List<string>), 400)]
        public async Task<IActionResult> GetListAsync(GetFilmFilterViewModel filter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.FirstError());
            }

            var entities = await _filmsService.SearchAsync(filter.Count, filter.Offset, filter.SearchString);

            return Ok(EntitiesToViewModels(entities));
        }

        /// <summary>
        /// Get count of films by search string.
        /// </summary>
        /// <response code="200">Success.</response>
        [AllowAnonymous]
        [HttpGet("SearchCount")]
        [ProducesResponseType(typeof(int), 200)]
        public async Task<IActionResult> CountAsync(GetFilmFilterViewModel filter)
        {
            return Ok(await _filmsService.SearchCountAsync(filter.SearchString));
        }

        /// <summary>
        /// Get count of films.
        /// </summary>
        /// <response code="200">Success.</response>
        [AllowAnonymous]
        [HttpGet("Count")]
        [ProducesResponseType(typeof(int), 200)]
        public async Task<IActionResult> CountAsync()
        {
            return Ok(await _filmsService.CountAsync());
        }

        /// <summary>
        /// Get film by id.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Failed to get film. Error in response body.</response>
        /// <response code="404">The film with the received id was not found.</response>
        [AllowAnonymous]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(FilmGetViewModel), 200)]
        [ProducesResponseType(typeof(List<string>), 400)]
        public async Task<IActionResult> GetAsync(int id)
        {
            if (id < 1)
            {
                return BadRequest("Id can not be less than 1.");
            }

            var entity = await _filmsService.GetByIdOrDefaultAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            return Ok(new FilmGetViewModel(entity, GetCoverUrl(entity)));
        }

        /// <summary>
        /// Chang film.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="viewModel"></param>
        /// <response code="200">The film was changed.</response>
        /// <response code="400">Failed to change film. Error in response body.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="403">You are not the administrator.</response>
        /// <response code="404">The film with the received id was not found.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(FilmGetViewModel), 200)]
        [ProducesResponseType(typeof(List<string>), 400)]
        public async Task<IActionResult> PutAsync(int id, [FromForm] FilmPutViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.FirstError());
            }

            var filmEntity = await _filmsService.GetByIdOrDefaultAsync(id);
            if (filmEntity == null)
            {
                return NotFound();
            }

            filmEntity.Description = viewModel.Description;
            filmEntity.Director = viewModel.Director;
            filmEntity.Name = filmEntity.Name;
            filmEntity.Year = filmEntity.Year;
            var oldCoverId = filmEntity.CoverId;
            if (viewModel.Cover != null)
            {
                ServiceResult addCoverResult;

                using (var stream = viewModel.Cover.OpenReadStream())
                {
                    addCoverResult = await _fileService.SaveImage(stream, ConfigureService.ConfigureCore.ImagesConfig, ImageExtensions.Jpeg, User.GetUserId(), viewModel.Cover.Name);
                }

                if (!addCoverResult.Succeeded)
                {
                    return addCoverResult.ToActionResult();
                }
                filmEntity.CoverId = addCoverResult.GetResultData<File>().Id;
            }

            var updateResult = await _filmsService.UpdateAsync(filmEntity);
            if (!updateResult.Succeeded)
            {
                await _fileService.DeleteAsync(oldCoverId);
            }
            else
            {
                await _fileService.DeleteAsync(filmEntity.CoverId);
            }

            return updateResult.ToObjectActionResult(new FilmGetViewModel(filmEntity, GetCoverUrl(filmEntity)));
        }

        /// <summary>
        /// Create new film.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <response code="201">The film was created.</response>
        /// <response code="400">Failed to create film. Error in response body.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="403">You are not the administrator.</response>
        [HttpPost]
        [ProducesResponseType(typeof(Film), 201)]
        [ProducesResponseType(typeof(List<string>), 400)]
        public async Task<IActionResult> PostAsync([FromForm] FilmPostViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.FirstError());
            }

            ServiceResult addCoverResult;

            using (var stream = viewModel.Cover.OpenReadStream())
            {
                addCoverResult = await _fileService.SaveImage(stream, ConfigureService.ConfigureCore.ImagesConfig, ImageExtensions.Jpeg, User.GetUserId(), viewModel.Cover.Name);
            }

            if (!addCoverResult.Succeeded)
            {
                return addCoverResult.ToActionResult();
            }

            var filmEntity = new Film
            {
                CoverId = addCoverResult.GetResultData<File>().Id,
                Description = viewModel.Description,
                Director = viewModel.Director,
                Name = viewModel.Name,
                Year = viewModel.Year
            };

            var createResult = await _filmsService.CreateAsync(filmEntity);
            if (!createResult.Succeeded)
            {
                await _fileService.DeleteAsync(filmEntity.CoverId);
            }

            return createResult.ToCreatedActionResult(Request.GetDisplayUrl() + $"/{filmEntity.Id}", new FilmGetViewModel(filmEntity, GetCoverUrl(filmEntity)));
        }

        /// <summary>
        /// Delete film.
        /// </summary>
        /// <param name="id">Id of film to delete.</param>
        /// <response code="200">The film was deleted.</response>
        /// <response code="400">Failed to delete film. Error in response body.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="403">You are not the administrator.</response>
        /// <response code="404">The film with the received id was not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.Admin)]
        [ProducesResponseType(typeof(string), 400)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id < 1)
            {
                return BadRequest("Id can not be less than 1");
            }

            var entity = await _filmsService.GetByIdOrDefaultAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            var result = await _filmsService.DeleteAsync(entity);

            return result.ToActionResult();
        }

        private List<FilmGetViewModel> EntitiesToViewModels(List<Film> entities)
        {
            var result = new List<FilmGetViewModel>();
            foreach (var entity in entities)
            {
                result.Add(new FilmGetViewModel(entity, GetCoverUrl(entity)));
            }

            return result;
        }

        private string GetCoverUrl(Film film)
        {
#pragma warning disable CS0162 // Unreachable code detected
#if DEBUG
            return "http://localhost:5500" + $"/Files/{film.CoverId}.JPEG";
#endif
            return Request.GetBaseUrl() + $"/Files/{film.CoverId}.JPEG";
#pragma warning restore CS0162 // Unreachable code detected
        }
    }
}
