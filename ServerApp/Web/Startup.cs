﻿using System.IO;
using Database;
using Domain.Core.Entities.Identity;
using Domain.Core.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Services;
using Services.Core;
using Services.Interfaces;
using Swashbuckle.AspNetCore.Swagger;
using Web.Core;
using Web.Core.Middleware;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConfigureService.ConfigureCore = new ConfigureCore(configuration, "DefaultConnection");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var configurator = new ServicesConfigurator(Configuration, services);

            configurator.ConfigureDbContextMySql<ApplicationDbContext>(ConfigureService.ConfigureCore.ConnectionString);

            configurator.ConfigureIdentity<ApplicationUser, UserRole, ApplicationDbContext>();

            configurator.ConfigureBearerAuthentication(
                6000,
                ConfigureService.ConfigureCore.AuthJwtOptions.Issuer,
                ConfigureService.ConfigureCore.AuthJwtOptions.Audience,
                ConfigureService.ConfigureCore.AuthJwtOptions.SymmetricSecurityKey);

            configurator.ConfigureSpaStaticFiles();

            configurator.ConfigureInjectionCoreService();

            configurator.ConfigureMvc(CompatibilityVersion.Version_2_1);

            var info = new Info
            {
                Title = "Angular & ASP.NET Core",
                Description = "Angular & ASP.NET Core",
                Contact = new Contact
                {
                    Name = "Dmitry Protko",
                    Email = "mirypoko@gmail.com",
                    Url = "https://github.com/mirypoko/AspNetCoreAndAngular"
                },
                Version = "v1"
            };

            configurator.ConfigureSwagger("v1", info, true);

            services.AddTransient<IFilmsService, FilmsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware<LogQueryMiddleware>();
            app.UseMiddleware<LogExceptionHandlerMiddleware>();
        
            loggerFactory.AddConsole(LogLevel.Trace);
            loggerFactory.AddDebug(LogLevel.Debug);
            loggerFactory.AddNLog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseAuthentication();

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web V1"); });

            app.UseHttpsRedirection();

            app.UseMvcWithDefaultRoute();

            //app.UseSpa(spa =>
            //{
            //    // To learn more about options for serving an Angular SPA from ASP.NET Core,
            //    // see https://go.microsoft.com/fwlink/?linkid=864501

            //    spa.Options.SourcePath = "ClientApp";

            //    if (env.IsDevelopment())
            //    {
            //        spa.UseAngularCliServer(npmScript: "start");
            //    }
            //});

            app.Run(async context =>
            {
                context.Response.ContentType = "text/html";
                await context.Response.SendFileAsync(Path.Combine(env.WebRootPath, "index.html"));
            });
        }
    }
}
