﻿using System.ComponentModel.DataAnnotations;
using Web.Core.ViewModels;

namespace Web.ViewModels
{
    public class GetFilmFilterViewModel : GetFilterViewModel
    {
        public string SearchString { get; set; }
    }
}
