﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.Film
{
    public class FilmGetViewModel
    {
        public FilmGetViewModel(Domain.Entities.Film film, string urlToCover)
        {
            Id = film.Id;
            Name = film.Name;
            Description = film.Description;
            Director = film.Director;
            Year = film.Year;
            CoverUrl = urlToCover;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Director { get; set; }

        public int Year { get; set; }

        public string CoverUrl { get; set; }
    }
}
