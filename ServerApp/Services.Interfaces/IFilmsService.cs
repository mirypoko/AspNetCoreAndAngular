﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Services.Interfaces.Core.Base;

namespace Services.Interfaces
{
    public interface IFilmsService : IBaseDataService<int, Film>
    {
        Task<int> SearchCountAsync(string searchString);
        Task<List<Film>> SearchAsync(int? count, int? offset, string searchString);
    }
}