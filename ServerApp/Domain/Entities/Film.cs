﻿using System.ComponentModel.DataAnnotations;
using Domain.Core.Entities;
using Domain.Core.Entities.Base;
using Newtonsoft.Json;

namespace Domain.Entities
{
    public class Film : EntityBase<int>
    {
        [Required] 
        public string Name { get; set; }

        [Required] 
        public string CoverId { get; set; }

        [JsonIgnore] 
        public File Cover { get; set; }

        [Required] 
        public string Description { get; set; }

        [Required] 
        public string Director { get; set; }

        public int Year { get; set; }
    }
}