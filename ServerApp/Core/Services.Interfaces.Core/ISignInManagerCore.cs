﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.Core.Entities.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Services.Interfaces.Core
{
    public interface ISignInManagerCore
    {
        Task<ClaimsPrincipal> CreateUserPrincipalAsync(ApplicationUser user);
        bool IsSignedIn(ClaimsPrincipal principal);
        Task<bool> CanSignInAsync(ApplicationUser user);
        Task RefreshSignInAsync(ApplicationUser user);
        Task SignInAsync(ApplicationUser user, bool isPersistent, string authenticationMethod);
        Task SignInAsync(ApplicationUser user, AuthenticationProperties authenticationProperties, string authenticationMethod);
        Task SignOutAsync();
        Task<ApplicationUser> ValidateSecurityStampAsync(ClaimsPrincipal principal);
        Task<ApplicationUser> ValidateTwoFactorSecurityStampAsync(ClaimsPrincipal principal);
        Task<bool> ValidateSecurityStampAsync(ApplicationUser user, string securityStamp);
        Task<SignInResult> PasswordSignInAsync(ApplicationUser user, string password, bool isPersistent, bool lockoutOnFailure);
        Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure);
        Task<SignInResult> CheckPasswordSignInAsync(ApplicationUser user, string password, bool lockoutOnFailure);
        Task<bool> IsTwoFactorClientRememberedAsync(ApplicationUser user);
        Task RememberTwoFactorClientAsync(ApplicationUser user);
        Task ForgetTwoFactorClientAsync();
        Task<SignInResult> TwoFactorRecoveryCodeSignInAsync(string recoveryCode);
        Task<SignInResult> TwoFactorAuthenticatorSignInAsync(string code, bool isPersistent, bool rememberClient);
        Task<SignInResult> TwoFactorSignInAsync(string provider, string code, bool isPersistent, bool rememberClient);
        Task<ApplicationUser> GetTwoFactorAuthenticationUserAsync();
        Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent);
        Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent, bool bypassTwoFactor);
        Task<IEnumerable<AuthenticationScheme>> GetExternalAuthenticationSchemesAsync();
        Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string expectedXsrf);
        Task<IdentityResult> UpdateExternalAuthenticationTokensAsync(ExternalLoginInfo externalLogin);
        AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl, string userId);
        ILogger Logger { get; set; }
        UserManager<ApplicationUser> UserManager { get; set; }
        IUserClaimsPrincipalFactory<ApplicationUser> ClaimsFactory { get; set; }
        IdentityOptions Options { get; set; }
        HttpContext Context { get; set; }
    }
}