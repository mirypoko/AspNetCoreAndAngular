﻿using System.IO;
using System.Threading.Tasks;
using Domain.Core.Enums;
using Domain.Core.Models;
using Services.Interfaces.Core.Base;

namespace Services.Interfaces.Core
{
    public interface IFilesServiceCore : IBaseDataService<string, Domain.Core.Entities.File>
    {
        Task<ServiceResult> SaveImage(Stream input, ImageConfig imageConfig, ImageExtensions extension, string ownerId, string name = null);

        Task<ServiceResult> SaveImage(byte[] input, ImageConfig imageConfig, ImageExtensions imageExtension,
            string ownerId, string name = null);
    }
}