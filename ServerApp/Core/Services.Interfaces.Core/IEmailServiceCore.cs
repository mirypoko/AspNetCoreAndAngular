﻿using System.Threading.Tasks;
using Domain.Core.Models;

namespace Services.Interfaces.Core
{
    /// <summary>
    /// Service for sending emails.
    /// </summary>
    public interface IEmailServiceCore
    {
        /// <summary>
        /// Sends a message to the specified address.
        /// </summary>
        /// <param name="email">Email address of recipient.</param>
        /// <param name="subject">Subject of email.</param>
        /// <param name="htmlMessage">Email message.</param>
        /// <param name="mailOptions">Mail options.</param>
        /// <returns></returns>
        void SendEmail(string email, string subject, string htmlMessage, MailOptions mailOptions);
    }
}