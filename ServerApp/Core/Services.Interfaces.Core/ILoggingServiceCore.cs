﻿using Domain.Core.Entities.Logging;
using Services.Interfaces.Core.Base;

namespace Services.Interfaces.Core
{
    /// <summary>
    /// Provides the APIs for managing server events in a persistence store.
    /// </summary>
    public interface ILoggingServiceCore : IBaseDataService<long, ServerEvent>
    {
    }
}