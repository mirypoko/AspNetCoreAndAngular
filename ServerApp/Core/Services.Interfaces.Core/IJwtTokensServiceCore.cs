﻿using System.Threading.Tasks;
using Domain.Core.Entities.Identity;
using Domain.Core.Models.Identity;

namespace Services.Interfaces.Core
{
    public interface IJwtTokensServiceCore
    {
        Task<JwtToken> GetJwtTokenByRefreshTokenAsync(string refreshToken);
        Task<JwtToken> GetJwtTokenAsync(ApplicationUser user);
        Task DeleteUserRefreshTokensAsync(ApplicationUser user);
    }
}