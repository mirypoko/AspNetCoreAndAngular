﻿using Domain.Core.Entities.Identity;
using Domain.Core.Entities.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Database.Core
{
    public abstract class BaseDbContext : IdentityDbContext<ApplicationUser, UserRole, int>
    {
        protected BaseDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ServerEvent> ServerLog { get; set; }

        public DbSet<JwtRefreshToken> JwtRefreshTokens { get; set; }

        public DbSet<Domain.Core.Entities.File> Files { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<JwtRefreshToken>().HasIndex(t => t.Id).IsUnique();
        }
    }
}
