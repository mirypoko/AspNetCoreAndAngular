﻿using System.Collections.Generic;
using Domain.Core.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Services.Interfaces.Core;

namespace Services.Core
{
    public class RoleManagerCore : RoleManager<UserRole>, IRoleManagerCore
    {
        public RoleManagerCore(IRoleStore<UserRole> store, IEnumerable<IRoleValidator<UserRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<UserRole>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }
}