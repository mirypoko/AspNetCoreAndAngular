﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Core.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Services.Core.Extensions;
using Services.Interfaces.Core;

namespace Services.Core
{
    public class UserManagerCore : UserManager<ApplicationUser>, IUserManagerCore
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserManagerCore(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger, IHttpContextAccessor httpContextAccessor) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public virtual Task<ApplicationUser> GetCurrentUserAsync()
        {
            return FindByNameAsync(_httpContextAccessor.HttpContext.User.Identity.Name);
        }

        public Task<ApplicationUser> FindByNameOrEmailAsync(string userNameOrEmail)
        {
            var normalized = userNameOrEmail.NormalizedTrimAndUpper();
            return this.Users.SingleOrDefaultAsync(_ => _.NormalizedUserName == normalized || _.UserName == normalized);
        }
    }
}
