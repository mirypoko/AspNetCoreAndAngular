﻿using System.Collections.Generic;
using System.Linq;
using Domain.Core.Entities.Identity;
using Domain.Core.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Services.Interfaces.Core;

namespace Services.Core
{
    public class SignInManagerCore : SignInManager<ApplicationUser>, ISignInManagerCore
    {
        public SignInManagerCore(UserManager<ApplicationUser> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<ApplicationUser>> logger, IAuthenticationSchemeProvider schemes) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        {
        }

        private static ServiceResult SingInResultToServiceResult(SignInResult signInResult)
        {
            if (signInResult.Succeeded)
            {
                return new ServiceResult(true);
            }

            var errors = new List<string>();

            if (signInResult.IsLockedOut)
            {
                errors.Add("User is lock");
            }

            if (signInResult.IsNotAllowed)
            {
                errors.Add("User is not allowed");
            }

            errors.Add("Login failed");

            return new ServiceResult(false, errors.First());
        }
    }
}