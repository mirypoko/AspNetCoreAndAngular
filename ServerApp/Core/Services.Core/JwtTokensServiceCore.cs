﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Database.Core;
using Domain.Core.Entities.Identity;
using Domain.Core.Models.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Services.Core.Utils;
using Services.Interfaces.Core;

namespace Services.Core
{
    public sealed class JwtTokensServiceCore : IJwtTokensServiceCore
    {
        private readonly BaseDbContext _dbContext;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IUserManagerCore _userManager;

        public JwtTokensServiceCore(
            IUserManagerCore userManager,
            BaseDbContext dbContext,
            IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<JwtToken> GetJwtTokenByRefreshTokenAsync(string refreshToken)
        {
            var jwtRefreshToken = await _dbContext.Set<JwtRefreshToken>().Include(t => t.ApplicationUser)
                .FirstOrDefaultAsync(t => t.TokenValue == refreshToken);


            if (jwtRefreshToken != null && !jwtRefreshToken.Used) //&& jwtRefreshToken.CreatedDate.AddMonths(1) < DateTime.UtcNow)
            {
                return new JwtToken
                {
                    AccessToken = await GetAccessTokenAsync(jwtRefreshToken.ApplicationUser),
                    RefreshToken = refreshToken,
                    UserId = jwtRefreshToken.ApplicationUserId,
                    Expires = GetExpires(),
                    Lifetime = ConfigureService.ConfigureCore.AuthJwtOptions.Lifetime
                };
            }

            return null;
        }

        public async Task<JwtToken> GetJwtTokenAsync(ApplicationUser user)
        {
            var jwtAccessToken = await GetAccessTokenAsync(user);

            var jwtRefreshToken = GetRefreshToken(user);

            await AddRefreshTokenToDbAsync(jwtRefreshToken);

            return new JwtToken
            {
                UserId = user.Id,
                AccessToken = jwtAccessToken,
                RefreshToken = jwtRefreshToken.TokenValue,
                Expires = GetExpires(),
                Lifetime = ConfigureService.ConfigureCore.AuthJwtOptions.Lifetime
            };
        }

        public async Task DeleteUserRefreshTokensAsync(ApplicationUser user)
        {
            var tokens = await _dbContext.Set<JwtRefreshToken>().Where(t => t.ApplicationUserId == user.Id).ToListAsync();
            _dbContext.Set<JwtRefreshToken>().RemoveRange(tokens);
            await _dbContext.SaveChangesAsync();
        }

        private static long GetExpires()
        {
            return (long) UnixTimeUtil.GetUnixTime(DateTime.UtcNow.AddMinutes(ConfigureService.ConfigureCore.AuthJwtOptions.Lifetime));
        }

        private async Task AddRefreshTokenToDbAsync(JwtRefreshToken jwtRefreshToken)
        {
            await _dbContext.Set<JwtRefreshToken>().AddAsync(jwtRefreshToken);
            await _dbContext.SaveChangesAsync();
        }

        private JwtRefreshToken GetRefreshToken(ApplicationUser user)
        {
            return new JwtRefreshToken
            {
                ApplicationUserId = user.Id,
                ClientIp = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                TokenValue = Guid.NewGuid().ToString(),
                CreatedDate = DateTime.UtcNow,
                UserAgent = GetUserAgentString()
            };
        }

        private string GetUserAgentString()
        {
            var collection = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"];
            var result = string.Empty;
            foreach (var value in collection)
            {
                result += "/n" + value;
            }

            return result;
        }

        private async Task<string> GetAccessTokenAsync(ApplicationUser user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            var identity = GetClaimsIdentity(user, userRoles);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                ConfigureService.ConfigureCore.AuthJwtOptions.Issuer,
                ConfigureService.ConfigureCore.AuthJwtOptions.Audience,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(ConfigureService.ConfigureCore.AuthJwtOptions.Lifetime)),
                signingCredentials: new SigningCredentials(ConfigureService.ConfigureCore.AuthJwtOptions.SymmetricSecurityKey,
                    SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private static ClaimsIdentity GetClaimsIdentity(ApplicationUser user, IEnumerable<string> userRoles)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(ClaimsIdentity.DefaultIssuer, ConfigureService.ConfigureCore.AuthJwtOptions.Issuer),
                new Claim("UserEmail", user.NormalizedEmail),
                new Claim("UserId", user.Id.ToString())
            };

            if (userRoles != null)
            {
                claims.AddRange(userRoles.Select(role => new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));
            }

            var claimsIdentity =
                new ClaimsIdentity(claims, "Token", "Role",
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}