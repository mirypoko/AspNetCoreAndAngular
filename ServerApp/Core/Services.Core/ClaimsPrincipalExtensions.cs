﻿using Domain.Core.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Web.Core.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static bool IsAdmin(this ClaimsPrincipal user)
        {
            return user.IsInRole(Roles.Admin);
        }

        public static string GetUserId(this ClaimsPrincipal user)
        {
            return user.Claims.Single(_ => _.Type == "UserId").Value;
        }
    }
}
