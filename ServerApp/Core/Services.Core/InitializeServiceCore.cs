﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Core.Constants;
using Domain.Core.Entities.Identity;
using Domain.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Services.Core.Extensions;
using Services.Interfaces.Core;

namespace Services.Core
{
    public static class InitializeServiceCore
    {
        public static void Initialize(IServiceProvider services, UserModel admin, List<UserModel> testUsers)
        {
            var roleServiceCore = services.GetRequiredService<IRoleManagerCore>();
            var userManager = services.GetRequiredService<IUserManagerCore>();
            RoleInitialize(roleServiceCore, Roles.Admin);
            UserInitialize(userManager, admin, Roles.Admin);
            TestUserInitialize(userManager, testUsers);
        }

        private static void TestUserInitialize(IUserManagerCore userManager, List<UserModel> users)
        {
            foreach (var user in users)
            {
                UserInitialize(userManager, user);
            }
        }

        private static void RoleInitialize(IRoleManagerCore roleService, string roleName)
        {
            var role = roleService.Roles.SingleOrDefaultAsync(_ => _.Name == roleName).Result;
            if (role == null)
            {
                var result = roleService.CreateAsync(new UserRole(roleName)).Result;
                if (!result.Succeeded) throw new Exception(result.Errors.Single().Description);
            }
        }

        private static void UserInitialize(IUserManagerCore userManager, UserModel userModel, string role = null)
        {
            var user = userManager.FindByNameAsync(userModel.UserName).Result;
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = userModel.UserName,
                    Email = userModel.Email
                };

                var result = userManager.CreateAsync(user, userModel.Password).Result.ToServiceResult();
                if (!result.Succeeded) throw new Exception(result.Message);

                if (role != null)
                    result = userManager.AddToRoleAsync(user, Roles.Admin).Result.ToServiceResult();
                if (!result.Succeeded) throw new Exception(result.Message);
            }
        }
    }
}