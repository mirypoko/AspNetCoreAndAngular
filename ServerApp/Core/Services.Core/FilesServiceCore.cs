﻿using System;
using System.IO;
using System.Threading.Tasks;
using Database;
using Database.Core;
using Domain.Core.Enums;
using Domain.Core.Models;
using Microsoft.EntityFrameworkCore;
using Services.Core.BaseServices;
using Services.Core.Utils;
using Services.Interfaces.Core;
using File = Domain.Core.Entities.File;

namespace Services.Core
{
    public class FilesServiceCore : BaseDataService<string, File>, IFilesServiceCore
    {
        public FilesServiceCore(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Task<ServiceResult> SaveImage(Stream input, ImageConfig imageConfig, ImageExtensions imageExtension, string ownerId, string name = null)
        {
            if (!ImageUtil.IsImage(input))
            {
                return Task.FromResult(new ServiceResult(false,
                    ServiceResult.ServiceResultType.Operation,
                    message: "Incorrect format of image."));
            }
            var img = new ImageUtil(input);
            return SaveImage(img, imageConfig, imageExtension, ownerId, name);
        }

        public Task<ServiceResult> SaveImage(byte[] input, ImageConfig imageConfig, ImageExtensions imageExtension, string ownerId, string name = null)
        {
            if (!ImageUtil.IsImage(input))
            {
                return Task.FromResult(new ServiceResult(false,
                    ServiceResult.ServiceResultType.Operation,
                    message: "Incorrect format of image."));
            }
            var img = new ImageUtil(input);
            return SaveImage(img, imageConfig, imageExtension, ownerId, name);
        }

        public Task<ServiceResult> SaveImage(ImageUtil image, ImageConfig imageConfig, ImageExtensions imageExtension, string ownerId, string name = null)
        {
            if (image.Height > imageConfig.MaxImageHeight)
            {
                return Task.FromResult(new ServiceResult(false,
                    ServiceResult.ServiceResultType.Operation,
                    message: $"Incorrect height of image: more than {imageConfig.MaxImageHeight} px"));
            }

            if (image.Height < imageConfig.MinImageHeight)
            {
                return Task.FromResult(new ServiceResult(false, ServiceResult.ServiceResultType.Operation,
                    message: $"Incorrect height of image: less than {imageConfig.MinImageHeight} px"));
            }

            if (image.Width > imageConfig.MaxImageWidth)
            {
                return Task.FromResult(new ServiceResult(false, ServiceResult.ServiceResultType.Operation,
                    message: $"Incorrect width of image: more than {imageConfig.MaxImageWidth} px"));
            }

            if (image.Width < imageConfig.MinImageWidth)
            {
                return Task.FromResult(new ServiceResult(false, ServiceResult.ServiceResultType.Operation,
                    message: $"Incorrect width of image: less than {imageConfig.MinImageWidth} px"));
            }

            byte[] imgBytes;

            switch (imageExtension)
            {
                case ImageExtensions.Bmp:
                    imgBytes = image.ToBmp();
                    break;
                case ImageExtensions.Gif:
                    imgBytes = image.ToGif();
                    break;
                case ImageExtensions.Jpeg:
                    imgBytes = image.ToJpeg(imageConfig.MaxQualityJpg);
                    break;
                case ImageExtensions.Png:
                    imgBytes = image.ToPng(imageConfig.MaxQualityPng);
                    break;
                default:
                    throw new ArgumentException(nameof(imageExtension));
            }

            if (imgBytes.Length > imageConfig.MaxImageSize)
            {
                return Task.FromResult(new ServiceResult(false, ServiceResult.ServiceResultType.Operation,
                    message: $"Incorrect size of image: more than {imageConfig.MaxImageSize} bytes"));
            }

            if (name == null)
            {
                name = imageExtension + " image";
            }

            return this.CreateAsync(new File()
            {
                Name = name,
                Data = imgBytes,
                Extension = imageExtension.ToString().ToUpper(),
                OwnerId = ownerId
            });
        }
    }
}