﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Core;
using Domain.Core.Entities.Base;
using Domain.Core.Models;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces.Core.Base;

namespace Services.Core.BaseServices
{
    public abstract class BaseDataService<TKey, TEntity> : IBaseDataService<TKey, TEntity>
        where TEntity : class, IEntityBase<TKey>
    {
        protected readonly BaseDbContext DbContext;

        protected BaseDataService(BaseDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual Task<TEntity> GetByIdOrDefaultAsync(TKey id)
        {
            return DbContext.Set<TEntity>().FindAsync(id);
        }

        public virtual Task<List<TEntity>> GetListAsync(int? count, int? offset)
        {
            var query = DbContext.Set<TEntity>().OrderBy(_ => _.Id).AsQueryable();
            if (offset.HasValue && offset > 0)
            {
                query = query.Skip(offset.GetValueOrDefault());
            }

            if (count.HasValue && count > 0)
            {
                query = query.Take(count.GetValueOrDefault());

            }
            return query.ToListAsync();
        }

        public virtual Task<int> CountAsync()
        {
            return DbContext.Set<TEntity>().CountAsync();
        }

        public virtual async Task<ServiceResult> CreateAsync(TEntity entity)
        {
            await DbContext.AddAsync(entity);
            await DbContext.SaveChangesAsync();
            return new ServiceResult(true, ServiceResult.ServiceResultType.Created, entity);
        }

        public virtual async Task<ServiceResult> DeleteAsync(TEntity entity)
        {
            DbContext.Remove(entity);
            await DbContext.SaveChangesAsync();
            return new ServiceResult(true);
        }

        public async Task<ServiceResult> DeleteAsync(TKey id)
        {
            DbContext.Remove(DbContext.Set<TEntity>().Find(id));
            await DbContext.SaveChangesAsync();
            return new ServiceResult(true);
        }

        public virtual async Task<ServiceResult> UpdateAsync(TEntity entity)
        {
            DbContext.Update(entity);
            await DbContext.SaveChangesAsync();
            return new ServiceResult(true, ServiceResult.ServiceResultType.Updated, entity);
        }
    }
}