﻿using System.Linq;
using Domain.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace Services.Core.Extensions
{
    public static class IdentityResultExtensions
    {
        public static ServiceResult ToServiceResult(this IdentityResult identityResult)
        {
            var errors = identityResult.Errors.Select(identityError => identityError.Description).ToList();
            return new ServiceResult(identityResult.Succeeded, errors.FirstOrDefault());
        }

        public static ServiceResult ToServiceResult(this IdentityResult identityResult,
            ServiceResult.ServiceResultType serviceResultType)
        {
            var errors = identityResult.Errors.Select(identityError => identityError.Description).ToList();
            return new ServiceResult(identityResult.Succeeded, serviceResultType, errors.FirstOrDefault());
        }
    }

}
