﻿namespace Services.Core.Extensions
{
    public static class StringExtensions
    {
        public static string NormalizedTrimAndUpper(this string str)
        {
            return str.Normalize().Trim().ToUpperInvariant();
        }
    }
}
