﻿using System.Linq;
using System.Security.Claims;
using Domain.Core.Constants;

namespace Services.Core.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static bool IsAdmin(this ClaimsPrincipal user)
        {
            return user.IsInRole(Roles.Admin);
        }

        public static string GetUserId(this ClaimsPrincipal user)
        {
            return user.Claims.Single(_ => _.Type == "UserId").Value;
        }
    }
}
