﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Domain.Core.Models;
using Services.Interfaces.Core;

namespace Services.Core
{
    public class EmailServiceCore : IEmailServiceCore
    {
        public void SendEmail(string email, string subject, string htmlMessage, MailOptions mailOptions)
        {
            var client = new SmtpClient(mailOptions.SmtpHost, mailOptions.SmtpPort)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(mailOptions.SenderName, mailOptions.SmtpPassword)
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress(mailOptions.SenderEmail)
            };
            mailMessage.To.Add(email);
            mailMessage.Body = htmlMessage;
            mailMessage.Subject = subject;
            client.Send(mailMessage);
        }
    }
}