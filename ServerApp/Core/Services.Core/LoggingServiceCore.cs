﻿using Database.Core;
using Domain.Core.Entities.Logging;
using Microsoft.EntityFrameworkCore;
using Services.Core.BaseServices;
using Services.Interfaces.Core;

namespace Services.Core
{
    public class LoggingServiceCore : BaseDataService<long, ServerEvent>, ILoggingServiceCore
    {
        public LoggingServiceCore(BaseDbContext dbContext) : base(dbContext)
        {
        }
    }
}