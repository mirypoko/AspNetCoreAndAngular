﻿using System.Linq;
using System.Threading.Tasks;
using Domain.Core.Entities.Identity;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces.Core;

namespace Web.Core.Controllers
{
    internal class CreateUserResult
    {
        public string AccessToken { get; set; }
        
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string UserPassword { get; set; }
    }

    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private const string TestDataPrefix = "testData32dfedf";

        private readonly IJwtTokensServiceCore _jwtTokenServices;

        private readonly ISignInManagerCore _signInManager;

        private readonly IUserManagerCore _userManager;

        public TestController(IUserManagerCore userManager,
            ISignInManagerCore signInManager,
            IJwtTokensServiceCore jwtTokenServices,
            IUserManagerCore applicationDbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtTokenServices = jwtTokenServices;
        }

        [HttpGet("Teapot")]
        public IActionResult Teapot()
        {
            this.HttpContext.Response.StatusCode = 418; // I'm a teapot
            return Json("I’m a teapot");
        }

        [HttpGet("Hello")]
        public IActionResult Hello()
        {
            return Ok("Hello!");
        }

        [HttpGet("RemoveTestData")]
        public async Task<IActionResult> RemoveTestData()
        {
            var users = _userManager.Users.Where(_ => _.UserName.Contains(TestDataPrefix)).ToList();
            foreach (var applicationUser in users)
            {
                await _userManager.DeleteAsync(applicationUser);
            }

            return Ok();
        }

        /// <summary>
        /// Create or get test user.
        /// </summary>
        /// <param name="userName"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Already created.</response>
        [HttpGet("CreateTestUser/{userName}")]
        public async Task<IActionResult> CreateTestUser(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = $"{TestDataPrefix}userName",
                    Email = $"{TestDataPrefix}{userName}@gmail.com",
                };

                await _userManager.CreateAsync(user, "12341234");
                user = await _userManager.FindByNameAsync($"{TestDataPrefix}userName");
            }
            else
            {
                return BadRequest("Already created");
            }

            var accessToken = await _jwtTokenServices.GetJwtTokenAsync(user);
            return Created(Request.GetDisplayUrl() + "/" + user.Id, new CreateUserResult
            {
                AccessToken = "Bearer " + accessToken.AccessToken,
                UserId = user.Id,
                UserName = user.UserName,
                UserPassword = "12341234"
            });
        }
    }
}