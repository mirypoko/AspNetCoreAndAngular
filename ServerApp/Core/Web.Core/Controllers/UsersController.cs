﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Domain.Core.Constants;
using Domain.Core.Entities.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Services.Interfaces.Core;
using Web.Core.Extensions;
using Web.Core.Logging;
using Web.Core.ViewModels;
using Web.Core.ViewModels.User;

namespace Web.Core.Controllers
{
    /// <summary>
    /// User API.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase, IRestController<int, ApplicationUser, UserPostViewModel, UserPutViewModel, GetFilterViewModel>
    {
        private readonly IEmailServiceCore _emailService;

        private readonly IJwtTokensServiceCore _jwtTokensService;

        private readonly ILogger<UsersController> _logger;

        private readonly IUserManagerCore _userManager;

        /// <inheritdoc />
        public UsersController(
            IUserManagerCore userManager,
            IEmailServiceCore emailService,
            IJwtTokensServiceCore jwtTokensService,
            ILogger<UsersController> logger)
        {
            _userManager = userManager;
            _emailService = emailService;
            _jwtTokensService = jwtTokensService;
            _logger = logger;
        }

        /// <summary>
        /// Get count of users.
        /// </summary>
        /// <response code="200">Success.</response>
        [AllowAnonymous]
        [HttpGet("count")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CountAsync()
        {
            return Ok(await _userManager.Users.CountAsync());
        }

        /// <summary>
        /// Get list of users.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="400">Failed to get users. Error in response body.</response>
        [AllowAnonymous]
        [HttpGet]
        [ProducesResponseType(typeof(List<UserGetViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetListAsync(GetFilterViewModel filter)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManager.Users.Skip(filter.Offset.GetValueOrDefault())
                    .Take(filter.Count.GetValueOrDefault()).ToListAsync();
                var viewModels = await ToViewModelsAsync(result);
                return Ok(viewModels);
            }

            return BadRequest(ModelState.FirstError());
        }

        /// <summary>
        /// Get user by id.
        /// </summary>
        /// <param name="id">Id of user.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Failed to get user. Error  in response body.</response>
        /// <response code="404">The user with the received id was not found.</response>
        [AllowAnonymous]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserGetViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAsync(int id)
        {
            var entity = await _userManager.FindByIdAsync(id.ToString());

            if (entity != null) return Ok(new UserGetViewModel(entity));

            return NotFound();
        }

        /// <summary>
        /// Get user roles.
        /// </summary>
        /// <param name="id">Id of user.</param>
        /// <response code="200">Success.</response>
        /// <response code="400">Failed to get user roles. Error in response body.</response>
        /// <response code="404">The user with the received id was not found.</response>
        [AllowAnonymous]
        [HttpGet("{id}/roles")]
        [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserRolesAsync(int id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null) return Ok(await _userManager.GetRolesAsync(user));

            return NotFound();
        }

        /// <summary>
        /// Chang password. In case of success, all user reset tokens will become invalid.
        /// </summary>
        /// <param name="putPasswordViewModel"></param>
        /// <returns></returns>
        /// <response code="200">The user password was changed.</response>
        /// <response code="400">Failed to change password. Error in response body.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="403">Access denied. (Attempt to change someone else's account)</response>
        /// <response code="404">User is not found.</response>
        [HttpPut("password")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PutPasswordAsync([FromBody] UserPasswordPutViewModel putPasswordViewModel)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.FirstError());

            var user = await _userManager.GetCurrentUserAsync();

            var result = await _userManager.ChangePasswordAsync(user, putPasswordViewModel.CurrentPassword,
                putPasswordViewModel.NewPassword);

            if (!result.Succeeded) return result.ToActionResult();

            _logger.LogWarning(LoggingEvents.UserChangedPassword, $"User {user.UserName} changed password");

            await _jwtTokensService.DeleteUserRefreshTokensAsync(user);

            return result.ToActionResult();
        }

        /// <summary>
        /// Chang user.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="putViewModel"></param>
        /// <response code="200">The user was changed.</response>
        /// <response code="400">Failed to change user. Error in response body.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="403">You are not the administrator.</response>
        /// <response code="404">User is not found.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UserGetViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> PutAsync(int id, [FromBody] UserPutViewModel putViewModel)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.FirstError());

            var user = await _userManager.GetCurrentUserAsync();
            if (user.Id != id)
            {
                if(!(await _userManager.IsInRoleAsync(user, Roles.Admin)))
                {
                    return Forbid();
                }

                user = await _userManager.FindByIdAsync(id.ToString());

                if (user == null)
                {
                    return NotFound();
                }
            }

            user.UserName = putViewModel.UserName;
            user.Email = putViewModel.Email;

            var currentPasswordCorrect = await _userManager.CheckPasswordAsync(user, putViewModel.CurrentPassword);

            if (!currentPasswordCorrect) return BadRequest("Current password is not correct.");

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded) return Ok(new UserGetViewModel(user));

            return result.ToActionResult();
        }

        /// <summary>
        /// Delete user.
        /// </summary>
        /// <param name="id">Id of user to delete.</param>
        /// <response code="200">The user was deleted.</response>
        /// <response code="400">Failed to delete user. Error in response body.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="403">You are not the administrator. (You can't change foreign account)</response>
        /// <response code="404">The user with the received id was not found.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user.Id != id)
            {
                if (!(await _userManager.IsInRoleAsync(user, Roles.Admin)))
                {
                    return Forbid();
                }

                user = await _userManager.FindByIdAsync(id.ToString());

                if (user == null)
                {
                    return NotFound();
                }
            }

            var result = await _userManager.DeleteAsync(user);
            return result.ToActionResult();
        }

        /// <summary>
        /// Create user.
        /// </summary>
        /// <param name="postViewModel"></param>
        /// <response code="201">Success.</response>
        /// <response code="400">Failed to create user. Error in response body.</response>
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(UserGetViewModel), 201)]
        [ProducesResponseType(typeof(string), 400)]
        public async Task<IActionResult> PostAsync([FromBody] UserPostViewModel postViewModel)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.FirstError());

            var user = new ApplicationUser
            {
                UserName = postViewModel.UserName,
                Email = postViewModel.Email
            };

            var result = await _userManager.CreateAsync(user, postViewModel.Password);

            if (!result.Succeeded) return result.ToActionResult();

            var viewModel = new UserGetViewModel(user);
            return Created(Request.GetDisplayUrl() + "/" + viewModel.Id, viewModel);
        }

        /// <summary>
        /// Get current user.
        /// </summary>
        /// <response code="200">Success.</response>
        /// <response code="401">User is not authorized.</response>
        /// <response code="404">User is not found.</response>
        [HttpGet("current")]
        [ProducesResponseType(typeof(UserGetViewModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CurrentAsync()
        {
            var user = await _userManager.GetCurrentUserAsync();

            if (user == null) return NotFound();

            var viewModel = new UserGetViewModel(user);
            var roles = await _userManager.GetRolesAsync(user);
            viewModel.Roles = new List<string>();
            foreach (var role in roles) viewModel.Roles.Add(role);

            return Ok(viewModel);
        }

        private async Task<List<UserGetViewModel>> ToViewModelsAsync(IEnumerable<ApplicationUser> users)
        {
            var result = new List<UserGetViewModel>();
            foreach (var user in users)
            {
                var roles = await _userManager.GetRolesAsync(user);
                result.Add(new UserGetViewModel(user, roles));
            }

            return result;
        }
    }
}