﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Core.Entities.Base;
using Microsoft.AspNetCore.Mvc;
using Web.Core.ViewModels;

namespace Web.Core.Controllers
{
    public interface IRestController<in TKey, TEntity, in TPostViewModel, in TPutViewModel, in TGetFilterViewModel> where TEntity : class, 
        IEntityBase<TKey> where TPostViewModel : class where TPutViewModel: class where TGetFilterViewModel: GetFilterViewModel
    {
        Task<IActionResult> CountAsync();

        Task<IActionResult> GetListAsync(TGetFilterViewModel filter);

        Task<IActionResult> GetAsync(TKey id);

        Task<IActionResult> PutAsync(TKey id, TPutViewModel putViewModel);

        Task<IActionResult> PostAsync(TPostViewModel postViewModel);

        Task<IActionResult> DeleteAsync(TKey id);
    }
}
