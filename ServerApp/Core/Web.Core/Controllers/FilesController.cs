﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Core;
using Services.Interfaces.Core;

namespace Web.Core.Controllers
{
    [Route("[controller]")]
    public class FilesController : ControllerBase
    {
        private readonly IFilesServiceCore _fileService;

        public FilesController(IFilesServiceCore fileService)
        {
            _fileService = fileService;
        }

        [HttpGet("{id}.{extension}")]
        public Task<IActionResult> GetAsync(string id, string extension)
        {
            var path = ConfigureService.ConfigureCore.FilesFolderPath + "//" + id + "." + extension;
            return ReturnFileAsync(path, id);
        }

        [HttpGet("{id}")]
        public Task<IActionResult> GetAsync(string id)
        {
            var path = ConfigureService.ConfigureCore.FilesFolderPath + "//" + id;
            return ReturnFileAsync(path, id);
        }

        private async Task<IActionResult> ReturnFileAsync(string filePath, string fileId)
        {
            if (ConfigureService.ConfigureCore.UseDriveToStoreFiles)
            {
                if (System.IO.File.Exists(filePath))
                {
                    return PhysicalFile(filePath, "application/octet-stream");
                }
            }


            var file = await _fileService.GetByIdOrDefaultAsync(fileId);

            if (file == null)
            {
                return NotFound();
            }

            if (ConfigureService.ConfigureCore.UseDriveToStoreFiles && !System.IO.File.Exists(filePath))
            {
                await System.IO.File.WriteAllBytesAsync(filePath, file.Data);
                return PhysicalFile(filePath, "application/octet-stream");
            }

            return File(file.Data, "application/octet-stream", file.Name);
        }
    }
}
