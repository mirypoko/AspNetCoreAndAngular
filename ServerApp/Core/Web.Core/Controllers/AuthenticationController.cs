﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Domain.Core.Constants;
using Domain.Core.Entities.Identity;
using Domain.Core.Models.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Services.Interfaces.Core;
using Web.Core.Extensions;
using Web.Core.Logging;
using Web.Core.ViewModels;
using Web.Core.ViewModels.User;


namespace Web.Core.Controllers
{
    /// <inheritdoc />
    [Authorize]
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IJwtTokensServiceCore _jwtTokenServices;

        private readonly ILogger<AuthenticationController> _logger;

        private readonly ISignInManagerCore _signInManager;

        private readonly IUserManagerCore _userManager;

        /// <inheritdoc />
        public AuthenticationController(
            IJwtTokensServiceCore jwtTokenServices,
            ISignInManagerCore signInManager,
            IUserManagerCore userManager,
            ILogger<AuthenticationController> logger)
        {
            _jwtTokenServices = jwtTokenServices;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
        }

        /// <summary>
        /// User is authenticated.
        /// </summary>
        /// <response code="200">Success.</response>
        [HttpGet("IsAuthenticated")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(bool), (int) HttpStatusCode.OK)]
        public bool IsAuthenticatedAsync()
        {
            return HttpContext.User.Identity.IsAuthenticated;
        }

        /// <summary>
        /// Get jwt token.
        /// </summary>
        /// <param name="model"></param>
        /// <response code="200">Success.</response>
        /// <response code="400">Invalid request. Error in response body.</response>
        /// <response code="403">Invalid password.</response>
        /// <response code="404">User with received email or name not found.</response>
        [AllowAnonymous]
        [ProducesResponseType(typeof(string), (int) HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(JwtToken), (int) HttpStatusCode.OK)]
        [HttpPost("GetJwtToken")]
        public async Task<IActionResult> GetJwtToken([FromBody] SingInViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.FirstError());

            var user = await _userManager.FindByNameOrEmailAsync(model.Username);

            if (user == null)
            {
                _logger.LogWarning(LoggingEvents.InvalidAuthorizationAttempt,
                    $"User with email or name {model.Username} not found.");

                return NotFound($"User with email or name {model.Username} not found");
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

            if (!result.Succeeded)
            {
                _logger.LogWarning(LoggingEvents.InvalidAuthorizationAttempt,
                    "Invalid authorization attempt.");

                return Forbid();
            }

            _logger.LogInformation(LoggingEvents.UserAuthorized, $"User (id = {user.Id}) has received Assess Token.");

            return Ok(await _jwtTokenServices.GetJwtTokenAsync(user));
        }

        /// <summary>
        /// Get new jwt access token by refresh token.
        /// </summary>
        /// <param name="refreshToken">Refresh jwt token.</param>
        /// <response code="200">New access token.</response>
        /// <response code="400">Failed to get new access jwt token.</response>
        [AllowAnonymous]
        [ProducesResponseType(typeof(JwtToken), 200)]
        [ProducesResponseType(typeof(List<string>), 400)]
        [HttpPost("UpdateJwtToken")]
        public async Task<IActionResult> UpdateJwtToken([FromBody] string refreshToken)
        {
            var token = await _jwtTokenServices.GetJwtTokenByRefreshTokenAsync(refreshToken);
            if (token == null) return BadRequest();

            _logger.LogInformation(LoggingEvents.UserUpdateJwtToken,
                "User updated jwt token.\n" +
                $"   Ip {HttpContext.Connection.RemoteIpAddress}");

            return Ok(token);
        }
    }
}