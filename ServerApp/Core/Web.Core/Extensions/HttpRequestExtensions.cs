﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.Extensions
{
    public static class HttpRequestExtensions
    {
        public static string GetBaseUrl(this HttpRequest request)
        {
            return $"{request.Scheme}://{request.Host}";
        }

        public static string GetBaseUrlOfController(this HttpRequest request)
        {
            return $"{request.GetBaseUrl()}{request.PathBase}";
        }
    }
}
