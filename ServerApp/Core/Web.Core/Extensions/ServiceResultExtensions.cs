﻿using System;
using Domain.Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace Web.Core.Extensions
{
    public static class ServiceResultExtensions
    {
        public static IActionResult ToCreatedActionResult(this ServiceResult servicesResult, string entityUrl, object viewModel = null)
        {
            return viewModel != null ? new CreatedResult(entityUrl, viewModel) : new CreatedResult(entityUrl, servicesResult.DataResult);
        }

        public static IActionResult ToObjectActionResult(this ServiceResult servicesResult, object viewModel = null)
        {
            return viewModel != null ? new OkObjectResult(viewModel) : new OkObjectResult(servicesResult.DataResult);
        }

        public static IActionResult ToActionResult(this ServiceResult servicesResult)
        {
            if (!servicesResult.Succeeded)
            {
                if (!string.IsNullOrWhiteSpace(servicesResult.Message))
                    return new BadRequestObjectResult(servicesResult.Message);
                return new BadRequestResult();
            }

            switch (servicesResult.ResultType)
            {
                case ServiceResult.ServiceResultType.Operation:
                    if (!string.IsNullOrWhiteSpace(servicesResult.Message))
                        return new OkObjectResult(servicesResult.Message);
                    return new OkResult();
                case ServiceResult.ServiceResultType.Forbidden:
                    return new ForbidResult();
                case ServiceResult.ServiceResultType.NotFound:
                    return new NotFoundResult();
                default:
                    throw new Exception("Unknown service result type");
            }
        }
    }
}