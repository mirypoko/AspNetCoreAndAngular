﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;

namespace Web.Core.Extensions
{
    public static class IdentityResultExtensions
    {
        public static IActionResult ToActionResult(this IdentityResult identityResult)
        {
            if (!identityResult.Succeeded)
            {
                if (EnumerableExtensions.Any(identityResult.Errors))
                    return new BadRequestObjectResult(identityResult.Errors.First());
                return new BadRequestResult();
            }

            return new OkResult();
        }
    }
}
