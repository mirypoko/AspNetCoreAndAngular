﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Web.Core.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        public static string FirstError(this ModelStateDictionary modelState)
        {
            return (from modelStateEntry in modelState.Values
                from error in modelStateEntry.Errors
                select error.ErrorMessage).First();
        }
    }
}