﻿using System.Collections.Generic;
using Domain.Core.Entities.Identity;

namespace Web.Core.ViewModels.User
{
    public class UserGetViewModel
    {
        public UserGetViewModel()
        {
        }

        public UserGetViewModel(ApplicationUser user, IList<string> roles = null)
        {
            Id = user.Id;
            UserName = user.UserName;
            Email = user.Email;
            Roles = roles;
        }

        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public IList<string> Roles { get; set; }
    }
}