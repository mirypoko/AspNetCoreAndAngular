﻿using System.ComponentModel.DataAnnotations;

namespace Web.Core.ViewModels.User
{
    public class SingInViewModel
    {
        [Required] 
        [MinLength(4)] 
        public string Username { get; set; }

        [MinLength(8)] 
        public string Password { get; set; }
    }
}