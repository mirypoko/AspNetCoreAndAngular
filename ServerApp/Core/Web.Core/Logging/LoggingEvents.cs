﻿namespace Web.Core.Logging
{
    public static class LoggingEvents
    {
        //Trace = 0

        //Debug - 1

        //Information - 2
        public const int GetItem = 2000;
        public const int GetListItems = 2001;
        public const int PostItem = 2002;
        public const int PutItem = 2003;
        public const int DeleteItem = 2004;

        public const int UserAuthorized = 2100;
        public const int UserUpdateJwtToken = 2101;

        //Warning - 3
        public const int InvalidAuthorizationAttempt = 3100;
        public const int UserChangedPassword = 3101;
        public const int InvalidUpdateJwtToken = 3102;

        public const int NotFound = 3000;
        public const int PostFailed = 3001;
        public const int PutFailed = 3002;
        public const int DeleteFailed = 3003;
        public const int BadRequest = 3004;

        //Errors - 4

        public const int AccessIsDenied = 4101;

        //Critical - 5

        public const int Exception = 5100;

        public const int ImATeapot = 5101;

        //None = 6
    }
}