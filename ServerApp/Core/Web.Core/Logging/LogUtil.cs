﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Domain.Core.Entities.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services.Core;
using Services.Interfaces.Core;

namespace Web.Core.Logging
{
    public class LogUtil
    {
        private readonly HttpContext _httpContext;

        private readonly ILogger _logger;

        private readonly ILoggingServiceCore _loggingServiceCore;

        private readonly LogLevel _logLevel;

        public LogUtil(ILogger logger, IHttpContextAccessor httpContextAccessor, ILoggingServiceCore loggingServiceCore, LogLevel logLevel)
        {
            _logger = logger;
            _loggingServiceCore = loggingServiceCore;
            _logLevel = logLevel;
            _httpContext = httpContextAccessor.HttpContext;
        }

        public Task GetAsync()
        {
            return AddLogRecordAsync(LoggingEvents.GetItem, LogLevel.Information);
        }

        public Task PostAsync()
        {
            return AddLogRecordAsync(LoggingEvents.PostItem, LogLevel.Information);
        }

        public Task PutAsync()
        {
            return AddLogRecordAsync(LoggingEvents.PutItem, LogLevel.Information);
        }

        public Task DeleteAsync()
        {
            return AddLogRecordAsync(LoggingEvents.DeleteItem, LogLevel.Information);
        }

        public Task AccessIsDeniedAsync()
        {
            return AddLogRecordAsync(LoggingEvents.AccessIsDenied, LogLevel.Error);
        }

        public Task NotFoundAsync()
        {
            return AddLogRecordAsync(LoggingEvents.NotFound, LogLevel.Warning);
        }

        public Task BadRequest()
        {
            return AddLogRecordAsync(LoggingEvents.BadRequest, LogLevel.Warning);
        }

        public async Task AddLogRecordAsync(int eventId, LogLevel logLevel)
        {
            if (_logLevel <= logLevel)
            {
                var infoForLog = GetInfoForLog(_httpContext);
                _logger.LogWarning(eventId, infoForLog);
                if (ConfigureService.ConfigureCore.SaveLogToDb)
                {
                    await _loggingServiceCore.CreateAsync(new ServerEvent()
                    {
                        LogLevel = (int)logLevel,
                        EventId = eventId,
                        Message = infoForLog
                    });
                }
            }
        }

        private static string GetInfoForLog(HttpContext httpContext)
        {
            return
                $"\r\nRequest: {FormatRequest(httpContext.Request)}\r\n" +
                $"Response: {FormatResponse(httpContext.Response)}\r\n" +
                $"Ip address: {httpContext.Connection.RemoteIpAddress}\r\n" +
                $"User: {GetUserNameForLog(httpContext.User)}\r\n";
        }

        private static string FormatResponse(HttpResponse response)
        {
            using (var memStream = new MemoryStream())
            {
                response.Body = memStream;

                memStream.Position = 0;
                var bodyAsText = new StreamReader(memStream).ReadToEnd();
                var messageObjToLog = new
                { cookies = response.Cookies, headers = response.Headers, requestBody = bodyAsText };
                return JsonConvert.SerializeObject(messageObjToLog);
            }
        }

        private static string FormatRequest(HttpRequest request)
        {
            string bodyAsText;

            if (request.ContentType != null && request.ContentType.Contains("form-data") && request.Form != null && request.Form.Keys.Any())
            {
                var strBuilder = new StringBuilder();
                foreach (var value in request.Form)
                {
                    strBuilder.Append($"{value.Key}:{value.Value}\r\n");
                }

                bodyAsText = strBuilder.ToString();
            }
            else
            {
                var body = request.Body;
                request.EnableRewind();
                var buffer = new byte[Convert.ToInt32(request.ContentLength)];
                request.Body.Read(buffer, 0, buffer.Length);
                bodyAsText = Encoding.UTF8.GetString(buffer);
                request.Body = body;
            }

            var messageObjToLog = new
            {
                scheme = request.Scheme,
                host = request.Host,
                path = request.Path,
                headers = request.Headers,
                queryString = request.Query,
                requestBody = bodyAsText
            };

            return JsonConvert.SerializeObject(messageObjToLog);
        }

        private static string GetUserNameForLog(ClaimsPrincipal user)
        {
            string result;
            if (!user.Identity.IsAuthenticated)
            {
                result = "Unidentified\r\n";
            }
            else
            {
                var userId = user.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value ??
                             "The identifier is missing in сlaims";
                result = $"{user.Identity.Name} (id = {userId})";
            }

            return result;
        }
    }
}