﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Services.Core;
using Services.Interfaces.Core;
using Web.Core.Logging;

namespace Web.Core.Middleware
{
    public class LogQueryMiddleware
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly ILogger<LogQueryMiddleware> _logger;

        private readonly RequestDelegate _next;

        public LogQueryMiddleware(RequestDelegate next, ILogger<LogQueryMiddleware> logger,
            IHttpContextAccessor httpContextAccessor)
        {
            _next = next;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task Invoke(HttpContext context, ILoggingServiceCore loggingServiceCore)
        {
            await _next(context);

            var logUtil = new LogUtil(_logger, _httpContextAccessor, loggingServiceCore, ConfigureService.ConfigureCore.LogLevel);

            if (context.Response.StatusCode < 400)
            {
                if (context.Request.Method == HttpMethods.Get)
                {
                    await logUtil.GetAsync();
                }
                else
                {
                    if (context.Request.Method == HttpMethods.Put)
                    {
                        await logUtil.PutAsync();
                    }
                    else
                    {
                        if (context.Request.Method == HttpMethods.Post)
                        {
                            await logUtil.PostAsync();
                        }
                        else
                        {
                            if (context.Request.Method == HttpMethods.Delete) await logUtil.DeleteAsync();
                        }
                    }
                }
            }
            else
            {
                switch (context.Response.StatusCode)
                {
                    case (int) HttpStatusCode.BadRequest:
                        await logUtil.BadRequest();
                        break;
                    case (int) HttpStatusCode.NotFound:
                        await logUtil.NotFoundAsync();
                        break;
                    case (int) HttpStatusCode.Forbidden:
                        await logUtil.AccessIsDeniedAsync();
                        break;
                }
            }
        }
    }
}