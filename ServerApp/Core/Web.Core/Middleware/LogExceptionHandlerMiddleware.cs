﻿using System;
using System.Net;
using System.Threading.Tasks;
using Domain.Core.Entities.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services.Core;
using Services.Interfaces.Core;
using Web.Core.Logging;

namespace Web.Core.Middleware
{
    public class LogExceptionHandlerMiddleware
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;

        public LogExceptionHandlerMiddleware(RequestDelegate next, ILogger<LogExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context, ILoggingServiceCore loggingServiceCore)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exp)
            {
                _logger.LogError(LoggingEvents.Exception, exp, exp.Message);
                if (ConfigureService.ConfigureCore.SaveLogToDb)
                {
                    await loggingServiceCore.CreateAsync(new ServerEvent()
                    {
                        LogLevel = (int)LogLevel.Critical,
                        EventId = LoggingEvents.Exception,
                        Message = exp.Message + ", StackTrace:" + exp.StackTrace
                    });
                }
                await HandleExceptionAsync(context, exp.GetBaseException());
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exp)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var result = JsonConvert.SerializeObject(new {Code = code, exp.Message, exp.StackTrace});

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) code;

            return context.Response.WriteAsync(result);
        }
    }
}