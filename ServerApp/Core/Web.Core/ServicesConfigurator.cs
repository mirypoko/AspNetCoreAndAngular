﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Database.Core;
using Domain.Core.Entities.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Services.Core;
using Services.Interfaces.Core;
using Swashbuckle.AspNetCore.Swagger;
using Web.Core.Extensions;

namespace Web.Core
{
    public class ServicesConfigurator
    {
        private readonly IServiceCollection _services;

        private readonly IConfiguration _configuration;

        public ServicesConfigurator(IConfiguration configuration, IServiceCollection services)
        {
            _services = services;
            _configuration = configuration;
        }

        public void ConfigureSwagger(string name, Info info, bool bearerAuthorization)
        {
            _services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc(name, info);

                config.OperationFilter<FormFileOperationFilter>();

                if (bearerAuthorization)
                {
                    config.AddSecurityDefinition("Bearer", new ApiKeyScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = "header",
                        Type = "apiKey"
                    });

                    var security = new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer", new string[] { }}
                    };

                    config.AddSecurityRequirement(security);
                }

                var basePath = AppContext.BaseDirectory;

                var assemblyName = Assembly.GetEntryAssembly().GetName().Name;
                var fileName = Path.GetFileName(assemblyName + ".xml");
                config.IncludeXmlComments(Path.Combine(basePath, fileName));
            });
        }

        public void ConfigureDbContextMySql<TDbContext>(string connectionString, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped, bool userLazyLoading = false) where TDbContext : BaseDbContext
        {
            _services.AddDbContext<TDbContext>(options =>
            {
                if (userLazyLoading)
                {
                    options.UseLazyLoadingProxies();
                }

                options.UseMySql(connectionString);
            }, serviceLifetime);

            _services.AddScoped<BaseDbContext, TDbContext>();
        }

        public void ConfigureDbContextSqlServer<TDbContext>(string connectionString, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped, bool userLazyLoading = false) where TDbContext : BaseDbContext
        {
            _services.AddDbContext<TDbContext>(options =>
            {
                if (userLazyLoading)
                {
                    options.UseLazyLoadingProxies();
                }

                options.UseSqlServer(connectionString);
            }, serviceLifetime);

            _services.AddScoped<BaseDbContext, TDbContext>();
        }

        public void ConfigureIdentity<TApplicationUser, TUserRole, TDbContext>(PasswordOptions passwordOptions = null, UserOptions userOptions = null) where TApplicationUser : ApplicationUser where TUserRole : UserRole where TDbContext : DbContext
        {
            if (passwordOptions == null)
            {
                passwordOptions = new PasswordOptions()
                {
                    RequireDigit = false,
                    RequiredLength = 8,
                    RequireLowercase = false,
                    RequireUppercase = false,
                    RequireNonAlphanumeric = false
                };
            }

            if (userOptions == null)
            {
                userOptions = new UserOptions { RequireUniqueEmail = true };
            }

            _services.AddIdentity<TApplicationUser, TUserRole>(options =>
                {
                    options.Password = passwordOptions;
                    options.User = userOptions;
                })
                .AddEntityFrameworkStores<TDbContext>()
                .AddDefaultTokenProviders();
        }

        public void ConfigureBearerAuthentication(int validationIntervalSeconds, string issuer, string audience, SymmetricSecurityKey symmetricSecurityKey)
        {
            _services.Configure<SecurityStampValidatorOptions>(options =>
                options.ValidationInterval = TimeSpan.FromSeconds(validationIntervalSeconds));

            _services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;

                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = issuer,

                        ValidateAudience = true,
                        ValidAudience = audience,

                        ValidateLifetime = true,

                        IssuerSigningKey = symmetricSecurityKey,
                        ValidateIssuerSigningKey = true,

                        ClockSkew = TimeSpan.Zero
                    };
                });
        }

        public void ConfigureMvc(CompatibilityVersion version)
        {
            _services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .SetCompatibilityVersion(version);
        }

        public void ConfigureInjectionCoreService()
        {
            _services.AddTransient<IJwtTokensServiceCore, JwtTokensServiceCore>();
            _services.AddTransient<IEmailServiceCore, EmailServiceCore>();
            _services.AddTransient<ILoggingServiceCore, LoggingServiceCore>();
            _services.AddTransient<IFilesServiceCore, FilesServiceCore>();

            _services.AddTransient<IRoleManagerCore, RoleManagerCore>();
            _services.AddTransient<IUserManagerCore, UserManagerCore>();
            _services.AddTransient<ISignInManagerCore, SignInManagerCore>();
        }

        public void ConfigureSpaStaticFiles(string rootPath = null)
        {
            if (rootPath == null)
            {
                rootPath = "wwwroot";
            }

            _services.AddSpaStaticFiles(configuration => { configuration.RootPath = rootPath; });
        }
    }
}
