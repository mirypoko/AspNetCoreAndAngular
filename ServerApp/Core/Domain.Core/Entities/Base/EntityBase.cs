﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Core.Entities.Base
{
    public abstract class EntityBase<TKey> : IEntityBase<TKey>
    {
        [Key] 
        public virtual TKey Id { get; set; }
    }
}