﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Domain.Core.Entities.Identity;

namespace Domain.Core.Entities.Base
{
    public class Token : EntityBase<int>
    {
        public DateTime CreatedDate { get; set; }

        public string TokenValue { get; set; }

        public bool Used { get; set; }

        [Required]
        public int ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        [Required]
        public string ClientIp { get; set; }

        [Required]
        public string UserAgent { get; set; }
    }
}
