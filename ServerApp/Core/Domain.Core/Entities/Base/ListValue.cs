﻿namespace Domain.Core.Entities.Base
{
    public abstract class ListValue : EntityBase<int>
    {
        public string Value { get; set; }
    }
}
