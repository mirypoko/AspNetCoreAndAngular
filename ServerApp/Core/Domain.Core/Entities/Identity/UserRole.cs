﻿using System.Collections.Generic;
using Domain.Core.Entities.Base;
using Microsoft.AspNetCore.Identity;

namespace Domain.Core.Entities.Identity
{
    public class UserRole : IdentityRole<int>, IEntityBase<int>
    {
        public UserRole(string roleName) : base(roleName) { }

        public UserRole() { }
    }
}