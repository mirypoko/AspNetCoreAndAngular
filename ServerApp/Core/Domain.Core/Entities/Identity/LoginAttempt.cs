﻿using System.ComponentModel.DataAnnotations;
using Domain.Core.Entities.Base;

namespace Domain.Core.Entities.Identity
{
    public class LoginAttempt : EntityBase<int>
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string ClientIp { get; set; }

        public bool Successful { get; set; }
    }
}
