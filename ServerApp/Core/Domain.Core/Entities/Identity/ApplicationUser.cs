﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Core.Entities.Base;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace Domain.Core.Entities.Identity
{
    public class ApplicationUser : IdentityUser<int>, IEntityBase<int>
    {
        [Required]
        [MinLength(3)]
        [MaxLength(15)]
        [RegularExpression("^[A-Za-z][A-Za-z0-9_$@}{()]*$")]
        public override string UserName { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(15)]
        [RegularExpression("^[A-Z][A-Z0-9_$@}{()]*$")]
        public override string NormalizedUserName { get; set; }
    }
}