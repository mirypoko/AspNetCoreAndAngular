﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Core.Entities.Base;

namespace Domain.Core.Entities.Logging
{
    public class ServerEvent : EntityBase<long>
    {
        public long? EventId { get; set; }

        public DateTime Time { get; set; } = DateTime.Now;

        [Required] 
        public int LogLevel { get; set; }

        [Required] 
        public string Message { get; set; }
    }
}