﻿using System.ComponentModel.DataAnnotations;
using Domain.Core.Entities.Base;
using Domain.Core.Entities.Identity;

namespace Domain.Core.Entities
{
    public class File : EntityBase<string>
    {
        [Required]
        public string Name { get; set; }

        public string Extension { get; set; }

        [Required]
        public byte[] Data { get; set; }

        public string OwnerId { get; set; }
        public ApplicationUser Owner { get; set; }
    }
}