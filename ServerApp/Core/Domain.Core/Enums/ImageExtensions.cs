﻿namespace Domain.Core.Enums
{
    public enum ImageExtensions
    {
        Jpeg,
        Gif,
        Png,
        Bmp
    }
}
