﻿using System;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Domain.Core.Models.Identity
{
    public class AuthJwtOptions
    {
        public string Issuer { get; private set; }

        public string Audience { get; private set; }

        public string Key { get; private set; }

        public int Lifetime { get; private set; }

        public SymmetricSecurityKey SymmetricSecurityKey { get; private set; }

        public AuthJwtOptions(string issuer, string audience, string key, int lifetime)
        {
            if (string.IsNullOrEmpty(issuer) || string.IsNullOrEmpty(audience) || string.IsNullOrEmpty(key) ||
                lifetime < 0) throw new Exception("Configuration error");

            Issuer = issuer;
            Audience = audience;
            Key = key;
            Lifetime = lifetime;
            SymmetricSecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}