﻿namespace Domain.Core.Models.Identity
{
    public class JwtToken
    {
        public int UserId { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public long Expires { get; set; }

        public int Lifetime { get; set; }
    }
}