﻿namespace Domain.Core.Models
{
    public class ImageConfig
    {
        public int MaxQualityPng { get; set; }

        public int MaxQualityJpg { get; set; }

        public int MaxImageSize { get; set; }

        public int MaxImageHeight { get; set; }

        public int MinImageHeight { get; set; }

        public int MinImageWidth { get; set; }

        public int MaxImageWidth { get; set; }

        public ImageConfig()
        {
        }

        public ImageConfig(int maxImageSize, int maxImageHeight, int minImageHeight, int minImageWidth, int maxImageWidth, int maxQualityJpg = 90, int maxQualityPng = 9)
        {
            MaxQualityJpg = maxQualityJpg;
            MaxImageSize = maxImageSize;
            MaxImageHeight = maxImageHeight;
            MinImageHeight = minImageHeight;
            MinImageWidth = minImageWidth;
            MaxImageWidth = maxImageWidth;
            MaxQualityPng = maxQualityPng;
        }
    }
}
