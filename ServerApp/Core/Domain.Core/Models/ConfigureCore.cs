﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Domain.Core.Models.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Domain.Core.Models
{
    public class ConfigureCore
    {
        public string FilesFolderPath { get; private set; }

        public MailOptions MailOptions { get; private set; }

        public LogLevel LogLevel { get; private set; }

        public UserModel AdminUserModel { get; private set; }

        public List<UserModel> TestUsersModels { get; private set; }

        public ImageConfig ImagesConfig { get; private set; }

        public string ConnectionString { get; private set; }
        public AuthJwtOptions AuthJwtOptions { get; private set; }

        public bool UseDriveToStoreFiles { get; private set; }

        public bool SaveLogToDb { get; private set; }

        public ConfigureCore(IConfiguration configuration, string connectionStringName)
        {
            SetImgSettings(configuration);
            SetFileFolder(configuration);
            SetJwtConfig(configuration);
            SetMailConfig(configuration);
            LogLevel = Enum.Parse<LogLevel>(configuration["Logging:LogLevel:Default"]);
            SetSystemUsersData(configuration);
            ConnectionString = configuration.GetConnectionString(connectionStringName);
            SaveLogToDb = bool.Parse(configuration["SaveLogToDb"]);
        }

        private void SetImgSettings(IConfiguration configuration)
        {
            ImagesConfig = configuration.GetSection("ImagesConfig").Get<ImageConfig>();
        }

        private void SetFileFolder(IConfiguration configuration)
        {
            FilesFolderPath = configuration["FilesFolderPath"];
            UseDriveToStoreFiles = bool.Parse(configuration["UseDriveToStoreFiles"]);
            if (UseDriveToStoreFiles && !Directory.Exists(FilesFolderPath))
            {
                Directory.CreateDirectory(FilesFolderPath);
            }
        }

        private void SetSystemUsersData(IConfiguration configuration)
        {
            AdminUserModel = configuration.GetSection("Admin").Get<UserModel>();
            TestUsersModels = configuration.GetSection("TestUsers").Get<UserModel[]>().ToList();
        }

        private void SetJwtConfig(IConfiguration configuration)
        {
            AuthJwtOptions = new AuthJwtOptions(
                configuration["Tokens:Issuer"],
                configuration["Tokens:Audience"],
                configuration["Tokens:Key"],
                Convert.ToInt32(configuration["Tokens:Lifetime"]));
        }

        private void SetMailConfig(IConfiguration configuration)
        {
            if (!int.TryParse(configuration["MailConfig:SmtpPort"], out var port) || port < 0)
                throw new Exception("Incorrect configuration");

            if (!bool.TryParse(configuration["MailConfig:UseSsl"], out var useSsl))
                throw new Exception("Incorrect configuration");

            MailOptions =
                new MailOptions(
                    configuration["MailConfig:SmtpHost"],
                    configuration["MailConfig:SmtpUserName"],
                    configuration["MailConfig:SmtpPassword"],
                    port,
                    useSsl,
                    configuration["MailConfig:SenderName"],
                    configuration["MailConfig:SenderEmail"]
                );
        }
    }
}
