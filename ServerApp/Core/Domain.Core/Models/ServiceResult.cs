﻿namespace Domain.Core.Models
{
    public sealed class ServiceResult
    {
        public enum ServiceResultType
        {
            Operation,
            NotFound,
            Forbidden,
            Created,
            Object,
            Updated
        }

        public ServiceResult(bool succeeded)
        {
            Succeeded = succeeded;
        }

        public ServiceResult(bool succeeded, string message)
        {
            Succeeded = succeeded;
            Message = message;
        }

        public ServiceResult(bool succeeded, ServiceResultType resultType = ServiceResultType.Operation,
            object dataResult = null, string message = null)
        {
            Succeeded = succeeded;
            Message = message;
            ResultType = resultType;
            DataResult = dataResult;
        }

        public bool Succeeded { get; }

        public string Message { get; }

        public object DataResult { get; }

        public T GetResultData<T>()
        {
            return (T) DataResult;
        }

        public ServiceResultType ResultType { get; } = ServiceResultType.Operation;
    }
}