﻿using UnitTests.Core.Models;

namespace UnitTests.Core
{
    internal static class Config
    {
        public const string PrefixOfTestData = "testData32dfedf";

        public const string BaseApiUrl = "https://aspnetcoreandangular.azurewebsites.net/api/";

        public static readonly SingInViewModel TestUserDate = new SingInViewModel
        {
            Password = "testUser123wg4e",
            Username = "testUser123wg4e"
        };

        public static readonly SingInViewModel AdminUserData = new SingInViewModel
        {
            Username = "Admin",
            Password = "12345678"
        };
    }
}