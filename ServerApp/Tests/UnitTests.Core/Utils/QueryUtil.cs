using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using UnitTests.Core.Models;

namespace UnitTests.Core.Utils
{
    public static class QueryUtil
    {
        public static JwtToken GetJwtToken(SingInViewModel singInViewModel)
        {
            var apiResult = SendQuery("Authentication/GetJwtToken", HttpMethod.Post,
                JsonConvert.SerializeObject(singInViewModel));
            return apiResult.HttpStatusCode == HttpStatusCode.OK
                ? JsonConvert.DeserializeObject<JwtToken>(apiResult.ContentAsString)
                : null;
        }

        public static ApiResult SendQuery(string uri, HttpMethod httpMethod, string body = null,
            Dictionary<string, string> queryStrings = null, Dictionary<string, string> headers = null,
            string accessToken = null)
        {
            var client = new HttpClient {BaseAddress = new Uri(Config.BaseApiUrl)};

            var requestUri = uri;
            if (queryStrings != null) requestUri = QueryHelpers.AddQueryString(uri, queryStrings);

            var request = new HttpRequestMessage(httpMethod, requestUri);
            if (headers != null && headers.Any())
                foreach (var header in headers)
                    request.Headers.Add(header.Key, header.Value);

            if (accessToken != null) request.Headers.Add("Authorization", "Bearer " + accessToken);

            if (body != null)
                request.Content = new StringContent(
                    body,
                    Encoding.UTF8,
                    "application/json"
                );

            return new ApiResult(client.SendAsync(request).Result);
        }
    }
}