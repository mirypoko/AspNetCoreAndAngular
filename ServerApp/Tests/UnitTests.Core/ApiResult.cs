﻿using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace UnitTests.Core
{
    public class ApiResult
    {
        public ApiResult(HttpResponseMessage responseMessage)
        {
            ContentAsString = responseMessage.Content.ReadAsStringAsync().Result;
            HttpStatusCode = responseMessage.StatusCode;
        }

        public HttpStatusCode HttpStatusCode { get; }

        public string ContentAsString { get; }

        public T ContentToObject<T>()
        {
            return JsonConvert.DeserializeObject<T>(ContentAsString);
        }
    }
}