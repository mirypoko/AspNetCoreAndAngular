﻿using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using UnitTests.Core.Models;
using UnitTests.Core.Utils;
using Xunit;

namespace UnitTests.Core.Tests
{
    public static class AuthenticationControllerTest
    {
        private const string ControllerName = "Authentication/";

        [Fact]
        public static void GetJwtToken()
        {
            var apiResult = QueryUtil.SendQuery(ControllerName + "GetJwtToken", HttpMethod.Post,
                JsonConvert.SerializeObject(Config.TestUserDate));

            Assert.Equal(HttpStatusCode.OK, apiResult.HttpStatusCode);
        }

        [Fact]
        public static void IsAuthenticated()
        {
            var jwtToken = QueryUtil.GetJwtToken(Config.TestUserDate);

            var apiResult = QueryUtil.SendQuery(ControllerName + "IsAuthenticated", HttpMethod.Get,
                accessToken: jwtToken.AccessToken);
            Assert.Equal("true", apiResult.ContentAsString);
        }

        [Fact]
        public static void IsNotAuthenticated()
        {
            var apiResult = QueryUtil.SendQuery(ControllerName + "IsAuthenticated", HttpMethod.Get,
                accessToken: Randomizer.GetRandomString(5));
            Assert.Equal("false", apiResult.ContentAsString);
        }

        [Fact]
        public static void UpdateJwtToken()
        {
            var jwtToken = QueryUtil.GetJwtToken(Config.TestUserDate);

            var apiResult = QueryUtil.SendQuery(ControllerName + "UpdateJwtToken", HttpMethod.Post,
                $"\"{jwtToken.RefreshToken}\"",
                accessToken: jwtToken.AccessToken);

            if (apiResult.HttpStatusCode == HttpStatusCode.OK)
            {
                jwtToken = JsonConvert.DeserializeObject<JwtToken>(apiResult.ContentAsString);
                apiResult = QueryUtil.SendQuery(ControllerName + "IsAuthenticated", HttpMethod.Get,
                    accessToken: jwtToken.AccessToken);
                Assert.Equal("true", apiResult.ContentAsString);
            }
            else
            {
                Assert.True(false);
            }
        }

        [Fact]
        public static void UpdateJwtTokenFalse()
        {
            var jwtToken = QueryUtil.GetJwtToken(Config.TestUserDate);

            var apiResult = QueryUtil.SendQuery(ControllerName + "UpdateJwtToken", HttpMethod.Post,
                $"\"{Randomizer.GetRandomString(5)}\"",
                accessToken: jwtToken.AccessToken);

            Assert.Equal(HttpStatusCode.BadRequest, apiResult.HttpStatusCode);
        }
    }
}