﻿using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using UnitTests.Core.Models;
using UnitTests.Core.Utils;
using Xunit;

namespace UnitTests.Core.Tests
{
    public static class UsersControllerTest
    {
        private const string ControllerName = "Users/";

        [Fact]
        public static void Current()
        {
            var jwtToken = QueryUtil.GetJwtToken(Config.TestUserDate);
            var apiResult = QueryUtil.SendQuery(ControllerName + "current", HttpMethod.Get,
                accessToken: jwtToken.AccessToken);
            if (apiResult.HttpStatusCode == HttpStatusCode.OK)
            {
                var currentUser = JsonConvert.DeserializeObject<UserGetViewModel>(apiResult.ContentAsString);
                Assert.Equal(Config.TestUserDate.Username, currentUser.UserName);
                return;
            }

            Assert.Equal(HttpStatusCode.OK, apiResult.HttpStatusCode);

        }

        [Fact]
        public static void Delete()
        {
            var userName = Config.PrefixOfTestData + Randomizer.GetRandomString(5);
            var postUserModel = new UserPostViewModel
            {
                UserName = userName,
                Email = userName + "@gmail.com",
                Password = userName
            };

            var apiResult = QueryUtil.SendQuery(ControllerName, HttpMethod.Post,
                JsonConvert.SerializeObject(postUserModel));
            if (apiResult.HttpStatusCode == HttpStatusCode.Created)
            {
                var jwtToken = QueryUtil.GetJwtToken(new SingInViewModel
                {
                    Username = userName,
                    Password = userName
                });
                apiResult = QueryUtil.SendQuery(ControllerName + "current", HttpMethod.Get,
                    accessToken: jwtToken.AccessToken);

                if (apiResult.HttpStatusCode == HttpStatusCode.OK)
                {
                    var userToDelete = JsonConvert.DeserializeObject<UserGetViewModel>(apiResult.ContentAsString);

                    var adminJwtToken = QueryUtil.GetJwtToken(Config.AdminUserData);

                    apiResult = QueryUtil.SendQuery(ControllerName + userToDelete.Id, HttpMethod.Delete,
                        accessToken: adminJwtToken.AccessToken);

                    Assert.Equal(HttpStatusCode.OK, apiResult.HttpStatusCode);
                    return;
                }
            }

            Assert.Equal(HttpStatusCode.Created, apiResult.HttpStatusCode);
        }

        [Fact]
        public static void PostUser()
        {
            var userName = Config.PrefixOfTestData + Randomizer.GetRandomString(5);
            var postUserModel = new UserPostViewModel
            {
                UserName = userName,
                Email = userName + "@gmail.com",
                Password = userName
            };

            var apiResult = QueryUtil.SendQuery(ControllerName, HttpMethod.Post,
                JsonConvert.SerializeObject(postUserModel));
            if (apiResult.HttpStatusCode == HttpStatusCode.Created)
            {
                var jwtToken = QueryUtil.GetJwtToken(new SingInViewModel
                {
                    Username = userName,
                    Password = userName
                });
                apiResult = QueryUtil.SendQuery(ControllerName + "current", HttpMethod.Get,
                    accessToken: jwtToken.AccessToken);

                if (apiResult.HttpStatusCode == HttpStatusCode.OK)
                {
                    var currentUser = JsonConvert.DeserializeObject<UserGetViewModel>(apiResult.ContentAsString);

                    Assert.Equal(currentUser.UserName, userName);
                    return;
                }
            }

            Assert.Equal(HttpStatusCode.Created, apiResult.HttpStatusCode);
        }

        [Fact]
        public static void PutUser()
        {
            var userName = Config.PrefixOfTestData + Randomizer.GetRandomString(5);
            var postUserModel = new UserPostViewModel
            {
                UserName = userName,
                Email = userName + "@gmail.com",
                Password = userName
            };

            var apiResult = QueryUtil.SendQuery(ControllerName, HttpMethod.Post,
                JsonConvert.SerializeObject(postUserModel));

            if (apiResult.HttpStatusCode == HttpStatusCode.Created)
            {
                var jwtToken = QueryUtil.GetJwtToken(new SingInViewModel
                {
                    Username = userName,
                    Password = userName
                });

                apiResult = QueryUtil.SendQuery(ControllerName + "current", HttpMethod.Get,
                    accessToken: jwtToken.AccessToken);

                if (apiResult.HttpStatusCode == HttpStatusCode.OK)
                {
                    var currentUser = JsonConvert.DeserializeObject<UserGetViewModel>(apiResult.ContentAsString);

                    var newUserName = currentUser.UserName + Randomizer.GetRandomString(2);

                    var putModel = new UserPutViewModel
                    {
                        CurrentPassword = userName,
                        Email = currentUser.Email,
                        UserName = newUserName
                    };

                    apiResult = QueryUtil.SendQuery(ControllerName + currentUser.Id, HttpMethod.Put,
                        JsonConvert.SerializeObject(putModel), accessToken: jwtToken.AccessToken);

                    if (apiResult.HttpStatusCode == HttpStatusCode.OK)
                    {
                        var changedUser = JsonConvert.DeserializeObject<UserGetViewModel>(apiResult.ContentAsString);

                        Assert.Equal(newUserName, changedUser.UserName);
                        return;
                    }
                }

                Assert.Equal(HttpStatusCode.OK, apiResult.HttpStatusCode);
            }

            Assert.Equal(HttpStatusCode.Created, apiResult.HttpStatusCode);
        }
    }
}