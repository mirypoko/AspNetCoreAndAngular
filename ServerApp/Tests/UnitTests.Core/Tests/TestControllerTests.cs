﻿using System.Net;
using System.Net.Http;
using UnitTests.Core.Utils;
using Xunit;

namespace UnitTests.Core.Tests
{
    public static class TestControllerTests
    {
        private const string ControllerName = "Test/";

        [Fact]
        public static void CreateTestUser()
        {
            var apiResult =
                QueryUtil.SendQuery(
                    ControllerName + "CreateTestUser/" + Config.PrefixOfTestData + Randomizer.GetRandomNumber(9999),
                    HttpMethod.Get);
            Assert.Equal(HttpStatusCode.Created, apiResult.HttpStatusCode);
        }

        [Fact]
        public static void Hello()
        {
            var apiResult = QueryUtil.SendQuery(ControllerName + "Hello", HttpMethod.Get);
            Assert.Equal("Hello!", apiResult.ContentAsString);
        }
    }
}