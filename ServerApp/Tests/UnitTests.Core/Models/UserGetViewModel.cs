﻿using System.Collections.Generic;

namespace UnitTests.Core.Models
{
    public class UserGetViewModel
    {
        public UserGetViewModel()
        {
        }

        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public IList<string> Roles { get; set; }
    }
}