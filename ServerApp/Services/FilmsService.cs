﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database;
using Domain.Core.Models;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Services.Core.BaseServices;
using Services.Interfaces;

namespace Services
{
    public class FilmsService : BaseDataService<int, Film>, IFilmsService
    {
        public FilmsService(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Task<int> SearchCountAsync(string searchString)
        {
            return DbContext.Set<Film>()
                .Where(f => f.Name.Contains(searchString) || f.Description.Contains(searchString) ||
                            f.Director.Contains(searchString)).CountAsync();
        }

        public Task<List<Film>> SearchAsync(int? count, int? offset, string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchString))
            {
                return GetListAsync(count, offset);
            }

            var query = DbContext.Set<Film>()
                .Where(f => f.Name.Contains(searchString) || f.Description.Contains(searchString) ||
                            f.Director.Contains(searchString))
                .Skip(offset.GetValueOrDefault());

            if (count == null) return query.ToListAsync();

            return query.Take(count.Value).ToListAsync();
        }

        public override async Task<ServiceResult> UpdateAsync(Film entity)
        {
            if (await DbContext.Set<Film>().AnyAsync(e => e.Name == entity.Name))
                return new ServiceResult(false, $"Name {entity.Name} already taken");
            return await base.UpdateAsync(entity);
        }

        public override async Task<ServiceResult> CreateAsync(Film entity)
        {
            if (await DbContext.Set<Film>().AnyAsync(e => e.Name == entity.Name))
                return new ServiceResult(false, $"Name {entity.Name} already taken");
            return await base.CreateAsync(entity);
        }
    }
}